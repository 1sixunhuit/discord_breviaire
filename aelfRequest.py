import sys
import re
import requests, json
from datetime import date, time, timedelta

from jsonReadFile import jsonFile2RawDatas

def str2date(date_string=None, format_us=None):
    # format us : YYYY MM DD

    if date_string is None:
        return None
    #endIf

    today = date.today() # j'en aurais potentiellement besoin plus tard

    splitDate_in = [e for e in re.split("-|/|_| |\xa0", date_string) if e]

    if len(splitDate_in)>1 and not ("hier" in date_string.lower()) and not ("demain" in date_string.lower()):
        # Recuperer l'annee
        pos_yr_in = 2  # De base, l'annee est en 3eme position, mais on va le verifier

        if len(splitDate_in) > 2:

            # Formattage US ou francais ?
            if format_us is None:
                format_us = (len(splitDate_in[0]) == 4)
            #endIf

            if format_us:
                pos_yr_in=0
            else:
                pos_yr_in=2
            #endIf

            if len(splitDate_in[pos_yr_in]) < 4:
                year="20{0:02d}".format(int(splitDate_in[pos_yr_in]))
            else:
                year=splitDate_in[pos_yr_in]
            #endIf

        else:
            year="{0:04d}".format(int(today.year))
        #endIf

        # Recuperer le jour
        if (pos_yr_in == 2)or(len(splitDate_in) < 3):
            pos_day_in=0
        else:
            pos_day_in=2
        #endIf

        day="{0:02d}".format(int(splitDate_in[pos_day_in]))


        # Recuperer le mois
        month_in = splitDate_in[1]

        all_months={
            "01":["janvier"  , "janv", "january" , "jan"  , "01","1"],
            "02":["fevrier"  , "fev" , "february", "febr" , "02","2"],
            "03":["mars"     , "mar" , "march"   ,          "03","3"],
            "04":["avril"    , "avr" , "april"   , "apr"  , "04","4"],
            "05":["mai"      ,         "may"     ,          "05","5"],
            "06":["juin"     ,         "june"             , "06","6"],
            "07":["juillet"  , "juil", "jully"   , "jul"  , "07","7"],
            "08":["aout"     ,         "august"  , "aug"  , "08","8"],
            "09":["septembre", "sept", "september",         "09","9"],
            "10":["octobre"  , "oct" , "october" ,          "10"],
            "11":["novembre" , "nov" , "november",          "11"],
            "12":["decembre" , "dec" , "december",          "12"]
        }

        for month_nb, values in all_months.items() :
            if month_in in values:
                month="{0:02d}".format(int(month_nb))
            #endIf
        #endFor

        return "{0:04d}-{1:02d}-{2:02d}".format(int(year),int(month),int(day))

    else:

        today = date.today()

        if date_string.lower() in ["hier","yesterday"]:
            the_date = today - timedelta(days=1)
        elif date_string.lower() in ["avant-hier","before-yesterday"]:
            the_date = today - timedelta(days=2)
        elif date_string.lower() in ["demain","tomorrow"]:
            the_date = today + timedelta(days=1)
        elif date_string.lower() in ["apres-demain", "après-demain","after-tomorrow"]:
            the_date = today + timedelta(days=2)
        else:
            sys.stderr.write("aelfRequest : Cas non implemente : {}\n".format(date_string))
        #endIf

        return the_date
    #endIf

    sys.stderr.write("Warning : unexpected case : I return a wrong date !\n")
    return "00-00-0000"





def aelf2datas(office_name, the_day=None, office_keys=None):
    # office_keys : Depreciated !

    # Initialisations
    if the_day is None or the_day in ["today",]:
        today = date.today()
        time="{0:04d}-{1:02d}-{2:02d}".format(int(today.year),int(today.month),int(today.day))
    else:
        time = str2date(date_string=the_day)
    #endIf

    zone="france"

    requested_url="https://api.aelf.org/v1/{0}/{1}/{2}".format(office_name,time,zone)

    url = requests.get(requested_url)
    text = url.text

    datas_from_aelf = json.loads(text)

    datas = datas_from_aelf[office_name]

    return datas
#endDef




def appendContentToAELF(name,datas):
    # Regarder s il y a des contenus supplementaires dans le fichier
    datas_content_additionnals =jsonFile2RawDatas( '{}.json'.format(name))

    if datas_content_additionnals is not None and datas is not None:
        if name in datas_content_additionnals.keys():
            for k, v in datas_content_additionnals[name].items():
                datas[k] = v
            #endFor
        #endIf
    #endIf

    return datas
#endDef


if __name__=="__main__":
    print(aelf2datas("informations"))
    print()
    print(aelf2datas("informations",the_day="3 juin"))
    print()
    print(aelf2datas("informations",the_day="hier"))
    print()
    print(aelf2datas("informations",the_day="avant-hier"))
    print()
    print(aelf2datas("informations",the_day="demain"))
    # str2date(date_string="01 juin")
    # str2date(date_string="01 juin 21")
    # str2date(date_string="01 juin 2021")

    # str2date(date_string="02/07")
    # str2date(date_string="2/7")
    # str2date(date_string="02/07/21")
    # str2date(date_string="02/07/2022")

    # str2date(date_string="2022/08/01")
    # str2date(date_string="21/08/01", format_us=True)
