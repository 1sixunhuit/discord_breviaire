import discord
import globals
import restrictions
from collect_datas import *
from preprint import datas2message
from bot_user import add_vote

def only_short_pray(contenu):
    retour=False
    for elt in ["court", "courte", "short", "quick", "rapide", "tout", "all", "toute", "full"]:
    #for elt in ["tout", "toute", "long", "all", "full"]
        retour=retour or elt in contenu.lower()
    #endFor
    return retour
#endDef


async def un_petit_retour(ctx, content="Le bot est actuellement en cours de tests.\nSi la moindre chose ne va pas, merci de le signaler avec \U0001F44E."):
    await add_vote(ctx=ctx,content=content)
    return 0


async def main_bot(ctx, office=None, priere=None, restrict=None, debug=None, **kwargs):

    if debug is None:
        debug = globals.debug
    #endIf

    allowed, msg = restrictions.allowUsage(ctx, restrict=restrict)
    if not allowed:
        await ctx.send(":warning: {0}".format(msg))
        return 6
    #endIf

    if priere is not None:

        if "yaedia" in priere:
            personnalisation_key="yaedia"
        else:
            personnalisation_key = None
        #endIf

        datas_content, datas_perso = json2datas(priere, debug=debug, personnalisation_key=personnalisation_key)
        datas_content = personnalisation_datas(datas_content, datas_perso)

    elif office is not None:

        datas_description = readJson('./content_office/{}.json'.format(office), key="deroulement")

        datas_content = aelf2datas(office_name=office, the_day="today")

        datas_content = collectDatas(name=office, progress_datas=datas_description, content_datas=datas_content, debug=debug, **kwargs)

    else:
         return 30
    #endIf
    datas = add_doxologie(datas_content)
    content=datas2message(datas, debug=debug, displayMarkdown=globals.displayMarkdown, **kwargs)

    if type(content) not in (list, tuple):
        content = [content,]
    #endIf

    if globals.sendInAFile:
        fname="/tmp/office_des_heures.md"
        with open(fname, "w") as f:
            f.write("\n# =========== New message ==========\n".join(content))
        #endWith
        await ctx.send(file=discord.File(fname))
    else:
        for es in content:
            if es is None:
                continue
            elif len(es)<LENMAX-4:
                await ctx.send("_ _\n"+es+"\n_ _")
            elif len(es)<=LENMAX:
                await ctx.send(es)
            else:
                sys.stderr.write(F"Warning : len = {len(es)} > {LENMAX} ! Split sauvage\n")
                write_log(
                    user="ERROR",
                    content=F"Warning : len = {len(es)} > {LENMAX} ! Split sauvage\n",
                    theFile=globals.logFile
                )
                await ctx.send(es[0:LENMAX])
                if len(es)<2*LENMAX:
                    await ctx.send(es[:LENMAX])
                    await ctx.send(es[LENMAX:])
                elif len(es)<3*LENMAX:
                    await ctx.send(es[:LENMAX])
                    await ctx.send(es[LENMAX:2*LENMAX])
                    await ctx.send(es[2*LENMAX:])
                else:
                    await ctx.send(es[:LENMAX])
                    await ctx.send(es[LENMAX:2*LENMAX])
                    await ctx.send(es[2*LENMAX:3*LENMAX])
                    await ctx.send(es[3*LENMAX:])
                #endIf
            #endIf
        #endFor
    #endIf

    if globals.askForReturn :
        await un_petit_retour(ctx)
    #endIf

    write_log(
        user=ctx.author,
        content="call '{0}' in channel '{1}' of the instance '{2}'".format(ctx.message.content, str(ctx.message.channel), str(ctx.message.guild.name)),
        theFile=globals.logFile
    )
    return 0
#endDef


