import os, sys
import requests, json

import globals


def readJson(filename, key=None, debug=True):

    try:
        with open(filename, 'r') as f:
            datas = json.load(f, strict=False)
        #endWith
    except Exception as e:
        datas=None
        if debug:
            sys.stderr.write("Error readJson : {}\n".format(filename))
            sys.stderr.write(str(e)+"\n")
            #exit(3)
        #endIf
    #endTry

    if key is None:
        return datas
    elif type(key) is str:

        if key in datas.keys():
            return datas[key]
        else:
            return {}
        #endIf
    elif type(key) is list or type(key) is tuple:
        return {v:k for k,v in datas.items() if k in key}
    else:
        sys.stderr.write("readJson : Unexpected error")
        raise TypeError("Only None, str, list, tuple are allowed for key, not {} while reading {}\n".format(type(key), filename))
    #endIf
#endDef


def jsonFile2RawDatas(name,
               personnalisation_key=None,
               debug=None,
               **kwargs):

    if len(name) < 5:
        pass
    elif name[-5:]==".json":
        name = name[:-5]
    #endIf

    # Aller chercher le fichier correspondant
    try:

        the_path = None

        len_dir = len(globals.all_content_paths)

        paths_exists = [
            os.path.exists(
                os.path.join(
                    each_dir,
                    "{0}.json".format(name)
                )
            )
            for each_dir in globals.all_content_paths
        ]

        the_dir = [globals.all_content_paths[i] for i in range(len_dir) if paths_exists[i]]

        if len(the_dir) > 1:
            sys.stderr.write("Warning : collectDatas : len the_path>1 : {} ; I will take the first".format(the_path))
            the_path = os.path.join(the_dir[0],"{0}.json".format(name))
        elif len(the_dir) == 1:
            the_path = os.path.join(the_dir[0],"{0}.json".format(name))
        else:
            return None, None
        #endIf

    except Exception as e:
        sys.stderr.write("collect_datas.py : Error while searching files (unexpected)\n"+str(e)+"\n")
        if(debug):
            sys.stderr.write("collect_datas 0708 - 1\n")
            exit(2)
        #endIf
    #endTry

    try:
        # Verifier que le fichier existe
        if the_path is None  or  not os.path.exists(the_path):

            write_log(
                user="Internal",
                content="collect_datas.py : contenuCompendium() : {name} not found but called".format(name=the_path)
            )

            if debug:
                exit(11)
            else:
                return ":warning: Internal error (contenuCompendium) : {name}".format(name=the_path)
            #endIf

        #endIf
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        if(debug):
            sys.stderr.write("collect_datas 0708 - 20\n")
            exit(2)
        #endIf
    #endTry

    try:

        content = readJson(the_path)

    except Exception as e:

        sys.stderr.write(":warning: Internal error (contenuCompendium) : readJson\n"+str(e)+"\n")

        if(debug):
            sys.stderr.write("collect_datas 0708 - 21\n")
            exit(2)
        #endIf
    #endTry

    return content
