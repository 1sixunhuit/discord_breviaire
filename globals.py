import os
import json

# Files :
global logFile
global configurationFile

# Bot :
global command_prefix
global name
global description

global token
global maintainer_id
global maintainer_name

command_prefix="?"
logFilePath="/tmp/log/"
logFilename="discord_breviaire.log"
configurationFile="./cfg/key.json"

# description du serveur
name="Bréviaire"
description = "Prières catholiques du jour provenant du site de l'AELF (aelf.org) plus quelques ajouts."

with open(configurationFile) as f:
    _datas = json.load(f)
    token = _datas['token']
    maintainer_id  = _datas['maintainer']['id']
    maintainer_name= _datas['maintainer']['name']

    if "command_prefix" in _datas.keys():
        command_prefix = _datas["command_prefix"]
    #endIf
    if "logFilePath" in _datas.keys():
        logFilePath = _datas["logFilePath"]
    #endIf
    if "logFilename" in _datas.keys():
        logFilename = _datas["logFilename"]
    #endIf
    _datas=None
#endwith

logFile = os.path.join(logFilePath,logFilename)
if not os.path.exists(logFilePath):
    os.makedirs(logFilePath)
#endIf


global restrictionFile
restrictionFile="./cfg/restrictions.json"

global content_office_path
global content_additionnals_path
global content_compendium_path
global content_rosary_path
content_office_path = './content_office'
content_additionnals_path= "./content_additionnals"
content_compendium_path = './content_compendium'
content_rosary_path = "./content_rosary"
if not os.path.exists(content_additionnals_path):
    os.makedirs(content_additionnals_path)
#endIf

global all_content_paths
all_content_paths = [
    content_additionnals_path,
    content_compendium_path,
    content_rosary_path,
    content_office_path
]


global bot_modules # Liste des modules existants a charger
bot_modules=["bot_admin", "bot_user","bot_breviaire","bot_compendium","bot_additionnals"]


global restrict_additionnals # Lire le contenu ajouté
restrict_additionnals ="restrict_additionnals"
global restrict_compendium   # Lire le contenu du compendium
restrict_compendium ="restrict_compendium"
global restrict_breviaire  # Lire le contenu du bréviaire
restrict_breviaire = "restrict_breviaire"
global restrict_lecture  # Lire le contenu de l'Evangile, des lectures, des psaumes (a implementer)
restrict_lecture = "restrict_lecture"

global restrict_user    # Sommaire, about, credits
restrict_user = "restrict_user"
global restrict_notify  # Envoyer une requete (bug, requete)
restrict_notify = "restrict_notify"

global restrict_publisher  # Ajouter du contenu
restrict_publisher = "restrict_publisher"
global restrict_maintainer  # Seul le mainteneur a le droit d'appeler de telles fonctions
restrict_maintainer = "restrict_maintainer"

global restrict_default
restrict_default = "restrict_default"


global debug
debug = False


global displayMarkdown # True pour copier/coller depuis le retour du bot dans un autre Discord, False si on utilise un fichier
displayMarkdown = False

global sendInAFile     # True pour creer un fichier a copier/coller dans un Discord
sendInAFile = False

global askForReturn
askForReturn = False

def initialize():
    pass
