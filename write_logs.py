import sys
import time

import globals

def write_log(content="", user=None, theFile=None):

    if theFile is None:
        theFile = globals.logFile
    #endIf

    now = time.ctime()

    if user is None:
        new_content="{date:<25}: {c}\n".format(
            date=str(now),
            c=content, u=user
        )
    else:
        new_content="{date:<25} (@{u}): {c}\n".format(
            date=str(now),
            c=content, u=user
        )
    #endIf

    if theFile is None:

        sys.stderr.write("Aucun fichier log n'a été configuré: ")
        sys.stderr.write(new_content)
        sys.stderr.write("\n")

    else:

        with open(theFile, "a") as f:
            f.write(new_content)
        #endWith

        sys.stdout.write("New log at {date:<28}\n".format(date=str(now)))
    #endIf

    return 0
#endDef

if __name__=="__main__":
    fname="/tmp/test.log"
    write_log(theFile=fname, content="Lancer le test")
    write_log(theFile=fname, content="Lancer le test", user="Me")
    with open(fname,"r") as f:
        content=f.readlines()
        print(content)
    #endwith
