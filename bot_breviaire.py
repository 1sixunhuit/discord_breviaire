from discord.ext import commands

import globals
import restrictions
from write_logs import *
from publish import main_bot, un_petit_retour
from preprint import evangile_request

# ================================================================== #
# ======================= FUNCTIONS ================================ #
# ================================================================== #


# ********************** TOC ************************************ #


class ToC(commands.Cog, name='Sommaire'):
    """Table des matières"""

    def __init__(self, bot):
        self.bot = bot
    #endDef


    @commands.command(
        name="sommaire",
        aliases=["toc"] ,
        brief='Prières disponibles',
        description='Prières disponibles dans le bréviaire, le compendium et parmis celles ajoutées'
    )
    async def sommaire(self, ctx):

        allowed, msg = restrictions.allowUsage(ctx, restrict=globals.restrict_user)
        if not allowed:
            await ctx.send(":warning: {0}".format(msg))
            return 6
        #endIf


        content=""".
* Offices
```    > {0}laudes
    > {0}lectures
    > {0}sixte
    > {0}none
    > {0}vepres
    > {0}complies                : avec confiteor du missel
    > {0}complies confiteor      : avec              missel
    > {0}complies missel         : avec              missel
    > {0}complies dominicain     : avec              dominicains
    > {0}complies sans confiteor : sans confiteor```

* Prières
```    > {0}angelus
    > {0}angelus_chant           : Angélus chanté
    > {0}regina_caeli
    > {0}reginacaeli             : alias pour (?regina_caeli)
    > {0}yaedia                  : Prière pour Yaedia (version longue)
    > {0}yaedia courte           :                    (version courte)```

* Autre :
```    > {0}evangile                : Évangile de la messe du jour```
""".format(globals.command_prefix)

        """
        * À venir :
        > ?messe            (To do)
        > ?psaume           (To do): Psaume de la messe du jour
        > ?salve_regina

        """

        await ctx.send(content)
    #endDef


# ********************** Offices ********************************** #


class Heures(
        commands.Cog,
        name="Liturgie des heures",
):
    def __init__(self, bot):
        self.bot = bot
    #endDef


    @commands.command(
        name="lectures",
        aliases=["lecture",],
        description="Office des lectures",
        ignore_extra=True
    )
    async def lectures(self, ctx):
        """Office des lectures"""
        await main_bot(ctx=ctx,office="lectures", restrict=globals.restrict_breviaire)
        return 0
    #endDef


    @commands.command(
        name="laudes",
        aliases=["laude",],
        description="Office des laudes",
        ignore_extra=True
    )
    async def laudes(self, ctx):
        """Office des laudes"""
        await main_bot(ctx=ctx,office="laudes", restrict=globals.restrict_breviaire)
        return 0
    #endDef


    @commands.command(
        name="tierce",
        aliases=["tierces",],
        description="Office tierce",
        ignore_extra=True
    )
    async def tierce(self, ctx):
        """Office tierce"""
        await main_bot(ctx=ctx,office="tierce", restrict=globals.restrict_breviaire)
        return 0
    #endDef


    @commands.command(
        name="sexte",
        aliases=["sextes", "sixte", "sixtes"],
        description="Office tierce",
        ignore_extra=True
    )
    async def sexte(self, ctx):
        """Office sexte"""
        await main_bot(ctx=ctx,office="sexte", restrict=globals.restrict_breviaire)
        return 0
    #endDef


    @commands.command(
        name="none",
        aliases=["nones",],
        description="Office none",
        ignore_extra=True
    )
    async def none(self, ctx):
        """Office none"""
        await main_bot(ctx=ctx,office="none", restrict=globals.restrict_breviaire)
        return 0
    #endDef

    @commands.command(
        name="vepres",
        aliases=["vêpres", "vepre" ,"vêpre"],
        description="Office des vêpres",
        ignore_extra=True
    )
    async def vepres(self, ctx):
        """Office des vêpres"""
        await main_bot(ctx=ctx,office="vepres", restrict=globals.restrict_breviaire)
        return 0
    #endDef


    @commands.command(
        name="complies",
        aliases=["complie", ],
        brief="Office des complies",
        description="Office des complies (arguments pour changer de confiteor : sans, dominicain, missel [default]"
    )
    async def complies(self, ctx):
        """Office des complies ;
'complies'            > confiteor classique
'complies missel'     > confiteor classique
'complies dominicain' > confiteor dominicain
'complies sans        > sans confiteor"""

        if(
                ("classique" in str(ctx.message.content).lower()) or
                ("sans" in str(ctx.message.content).lower())
        ):

            confiteor=None

        elif "dominicain" in str(ctx.message.content).lower():

            confiteor="dominicain"

        elif(
                ("missel" in str(ctx.message.content).lower()) or
                ("confiteor" in str(ctx.message.content).lower())
        ):

            confiteor="missel"

        else:

            #confiteor=None
            confiteor="missel"

        #endIf

        await main_bot(ctx=ctx,office="complies",confiteor=confiteor, restrict=globals.restrict_breviaire)
        return 0
    #endDef

#endClass



class Messe(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    #endDef

    # ............. Messe / Evangile ......................................
    # @bot.command()
    # async def messe(ctx):
    #     print(TODO)
    #     #content = getContentOffice("complies")
    #     content=["",]
    #     #for es in content:
    #     #    if es:
    #     await ctx.send("TODO")
    #     #endIf
    #     #endFor
    # #endDef

    @commands.command(
        name="evangile",
        aliases=["évangile",],
        description="Évangile de la messe du jour",
        ignore_extra=True
    )
    async def evangile(self, ctx):
        """Évangile de la messe du jour"""
        content = evangile_request("evangile", restrict=globals.restrict_lecture)

        for es in content:
            await ctx.send(".\n"+es+"\n.")
        #endFor

        await un_petit_retour(ctx)
    #endDef
#endClass

# .....................................................................


def setup(bot):
    bot.add_cog(ToC(bot))
    bot.add_cog(Heures(bot))
    bot.add_cog(Messe(bot))
