import discord
from discord.ext import commands

import time

import globals
import restrictions
from formatStrings import *
from write_logs import *

# class NotCircularImportError:
#     pass
# #endClass

# https://discord.com/developers/applications/983108100053696563/information
# https://dylanbonjean.wordpress.com/2018/01/05/bot-discord/

globals.initialize()

bot = commands.Bot(
    command_prefix=globals.command_prefix,
    description=globals.description,
    case_insensitive=True
)   # Attention case_insensitive : entraine baisse de perfo


# Change only the no_category default string
help_command = commands.DefaultHelpCommand(
    no_category = 'Commandes'
)



@bot.event
async def on_reaction_add(reaction, user):
    emoji = reaction.emoji

    # Si la reaction est celle d'un bot
    if user.bot:
        return

    if not reaction.message.author.bot :
        # si on reagit sur un humain
        return
    elif reaction.message.author.name != "Bréviaire":
        # si on reagit sur un autre bot
        return
    #endIf

    if(reaction.message.reference is None):
        # les reactions sur l'integralite des messages ne me concernent pas : seul les votes m'interessent.
        return
    #endIf

    log_reaction="Reaction {s} on the message posted at {t} by the bot from {uu} : {c}".format(
        t=str(reaction.message.created_at),
        s=str(reaction),
        uu=str(reaction.message.reference.resolved.author),
        c=str(reaction.message.reference.resolved.content)
    )

    write_log(user=user,
                      content=(log_reaction),
                      theFile=globals.logFile
                      )


    fixed_channel = bot.get_channel(reaction.message.reference.channel_id)

    maintainer_id = await bot.fetch_user(globals.maintainer_id)

    if emoji == "\U0001F44E": #thumbs down :thumbsdown:
        await fixed_channel.send("Merci de faire un retour détaillé des choses à améliorer sur le bot à {} ;)".format(maintainer_id.mention))
    #endIf


#endIf

# ================================================================== #
# =================== BOT'S EVENTS ================================= #
# ================================================================== #


@bot.event
async def on_ready():
    t = str(time.ctime())
    sys.stdout.write(F'Logged in as\n{bot.user.name}\n{bot.user.id}\n')
    sys.stdout.write(F'{t}\n---------\n\n')

    write_log(
        user=bot.user,
        content="Bot \'{}\' started".format(bot.user.name),
        theFile=globals.logFile
    )
#endDef


@bot.event
async def on_command_error(ctx, error):

    if isinstance(error, commands.CommandNotFound):

        await ctx.send(F""":warning: La commande `{ctx.message.content}` n'existe pas :
- Vérifier l'orthographe (il n'y a pas d'accents et autres caractères spéciaux, si ce n'est un "_")
- Utiliser `{globals.command_prefix}sommaire`  ou  `{globals.command_prefix}help` pour voir la liste des commandes disponible
- Si vous pensez que cette commande devrait être implémentée, merci de lancer la commande  :
.\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0`{globals.command_prefix}request {ctx.message.content} (autres arguments s'il y en a) : précisions`""")

        write_log(
            user=ctx.author,
            content="Command not found : \'{}\'".format(ctx.message.content),
        )
        sys.stderr.write(str(error))

    else:

        await ctx.send(F""":warning: Mauvais usage de la commande `{ctx.message.content}` :
- Vérifier la documentation pour voir comment elle fonctionne
- Si vous pensez que cet usage devrait être implémenté, merci de lancer la commande  :
.\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0`{globals.command_prefix}request {ctx.message.content} (arguments s'il y en a) : précisions`
-Si vous pensez que c'est un bug, merci de le signaler avec `?breviaire_bug`""")

        sys.stderr.write(str(ctx.command)+"\n"+str(ctx.message))
        write_log(
             user=ctx.author,
             content=F"Error raised with the command `{ctx.message.content}`",
         )

        raise error
    #endIf

    if "nones" in ctx.message.content:
        await ctx.send(F":arrow_right: Ne voulez-vous pas exécuter la commande `?none` (au singulier)  ?")
    elif "sixte" in ctx.message.content or "sixtes" in ctx.message.content or "sextes" in ctx.message.content:
        await ctx.send(F":arrow_right: Ne voulez-vous pas exécuter la commande `?sexte` (au singulier)  ?")
    #endIf

#endDef




# starting extensions

for extension in globals.bot_modules:
    try:
        bot.load_extension(extension)
    except Exception as e:
        exc = '{}: {}'.format(type(e).__name__, e)
        write_log(
            user="BOT",
            content='Failed to load extension {}\n{}'.format(extension, exc)
        )
        print('Failed to load extension {}\n{}'.format(extension, exc))
    #endTry
#endFor




# Attention : doit rester a la fin
bot.run(globals.token)

