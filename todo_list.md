# Titre stylé 8)

` ===================================================
  TTTTTT   OO      DDDD     OO   LL   II SS SS TTTTTT
    TT   OO  OO -- DD  DD OO  OO LL      SS      TT
    TT   OO  OO -- DD  DD OO  OO LL   II SS SS   TT
    TT   OO  OO    DD  DD OO  OO LL   II    SS   TT
    TT     OO      DDDD     OO   LLLL II SSSSS   TT
  ===================================================`

# Securiser le contenu :

* Modifier restrictions.py pour donner differents droits suivant les fct
centent_security/restrictions.json :
restrict_on
 * restrict_office       : appel offices
    * null, dans vocal_commentaires
 * restrict_specialPray  : appel priere pour yaedia :
    * null, dans vocal_commentaires
 * restrict_text         : les autres prieres (doxo, pater, ave, regina caeli, ...)
    * null, null
 * restrict_evangile     : l'evangile >> TRANSLATE
    * null
 * restrict_invoquer_bot : invoquer le bot >> TRANSLATE
 * restrict_editor       : ajouter d'autres prieres
    * maintainer (utiliser key.json)
 * restrict_maintainer   : uniquement le mainteneur
    * maintainer (utiliser key.json)


# Prieres a ajouter
* Utiliser beautifulsoupe pour virer les arguments des <p> ?

## Ave
<p style="text-align: center;">Je vous salue Marie, pleine de grâce ;<br />
Le Seigneur est avec vous.<br />
Vous êtes bénie entre toutes les femmes<br />
Et Jésus, le fruit de vos entrailles, est béni.<br />
Sainte Marie, Mère de Dieu,<br />
Priez pour nous pauvres pécheurs,<br />
Maintenant et à l&rsquo;heure de notre mort.</p>
<p style="text-align: center;">Amen</p>

## PAter
<p style="text-align: center;">Notre Père, qui es aux cieux,<br />
que ton nom soit sanctifié,<br />
que ton règne vienne,<br />
que ta volonté soit faite sur la terre comme au ciel.<br />
Donne-nous aujourd&rsquo;hui notre pain de ce jour.<br />
Pardonne-nous nos offenses,<br />
comme nous pardonnons aussi à ceux qui nous ont offensés.<br />
Et ne nous laisse pas entrer en tentation<br />
mais délivre-nous du Mal.</p>
<p style="text-align: center;">Amen</p>


## Credo NC
<p>Je crois en un seul Dieu, le Père tout puissant,<br />
créateur du du ciel et de la terre, de l&rsquo;univers visible et invisible,<br />
Je crois en un seul Seigneur, Jésus Christ,<br />
le Fils unique de Dieu, né du Père avant tous les siècles :<br />
Il est Dieu, né de Dieu,<br />
lumière, née de la lumière,<br />
vrai Dieu, né du vrai Dieu<br />
Engendré non pas créé,<br />
consubstantiel au Père ;<br />
et par lui tout a été fait.<br />
Pour nous les hommes, et pour notre salut,<br />
il descendit du ciel;<br />
Par l&rsquo;Esprit Saint, il a pris chair de la Vierge Marie, et s&rsquo;est fait homme.<br />
Crucifié pour nous sous Ponce Pilate,<br />
Il souffrit sa passion et fut mis au tombeau.<br />
Il ressuscita le troisième jour,<br />
conformément aux Ecritures, et il monta au ciel;<br />
il est assis à la droite du Père.<br />
Il reviendra dans la gloire, pour juger les vivants et les morts<br />
et son règne n&rsquo;aura pas de fin.<br />
Je crois en l&rsquo;Esprit Saint, qui est Seigneur et qui donne la vie;<br />
il procède du Père et du Fils.<br />
Avec le Père et le Fils, il reçoit même adoration et même gloire;<br />
il a parlé par les prophètes.</p>
<p>Je crois en l&rsquo;Eglise, une, sainte, catholique et apostolique.<br />
Je reconnais un seul baptême pour le pardon des péchés.<br />
J&rsquo;attends la résurrection des morts, et la vie du monde à venir.</p>
<p>Amen</p>



## Credo A
<p style="text-align: center;"><img loading="lazy" class="aligncenter" src="https://ec.cef.fr/wp-content/uploads/sites/2/cache/2014/05/cene/3898781467.jpg" data-watermark="Alain PINOGES/CIRIC" alt="" width="501" height="282" />Je crois en Dieu, le Père tout-puissant,<br />
Créateur du ciel et de la terre.<br />
Et en Jésus Christ, son Fils unique, notre Seigneur ;<br />
qui a été conçu du Saint Esprit, est né de la Vierge Marie,<br />
a souffert sous Ponce Pilate, a été crucifié,<br />
est mort et a été enseveli, est descendu aux enfers ;<br />
le troisième jour est ressuscité des morts,<br />
est monté aux cieux, est assis à la droite de Dieu le Père tout-puissant,<br />
d&rsquo;où il viendra juger les vivants et les morts.<br />
Je crois en l&rsquo;Esprit Saint, à la sainte Église catholique, à la communion des saints,<br />
à la rémission des péchés, à la résurrection de la chair, à la vie éternelle.</p>
<p style="text-align: center;">Amen</p>

## Contrition long
<p>Père, Dieu de tendresse et de <a href="https://eglise.catholique.fr/glossaire/misericorde" class="glossary-term"  title="Attitude qui incite &agrave; l&#039;indulgence et au pardon.">miséricorde</a>,<br />
j’ai <a href="https://eglise.catholique.fr/glossaire/peche" class="glossary-term"  title="Transgression volontaire d&#039;une r&egrave;gle ou d&#039;un commandement divin - point de rupture entre Dieu et l&#039;homme.">péché</a> contre Toi et mes frères.<br />
Je ne suis pas digne d’être appelé ton enfant,<br />
mais près de Toi se trouve le pardon.<br />
Accueille mon repentir.<br />
Que ton Esprit me donne la force<br />
de vivre selon ton amour<br />
en imitant Celui qui est mort pour nos péchés,<br />
Ton Fils Jésus-Christ Notre Seigneur.<br />
Mon Dieu, j&rsquo;ai un très grand regret de vous avoir offensé<br />
parce que vous êtes infiniment bon, infiniment aimable,<br />
et que le <a href="https://eglise.catholique.fr/glossaire/peche" class="glossary-term"  title="Transgression volontaire d&#039;une r&egrave;gle ou d&#039;un commandement divin - point de rupture entre Dieu et l&#039;homme.">péché</a> vous déplaît.<br />
Je prends la ferme résolution, avec le secours de votre sainte grâce<br />
de ne plus vous offenser et de faire <a href="https://eglise.catholique.fr/glossaire/penitence" class="glossary-term"  title="Conversion de l&#039;esprit et du coeur. Sacrement qui permet de recevoir le pardon des p&eacute;ch&eacute;s.">pénitence</a>.</p>

		</div>

## Contrition (court, classique)
Acte de contrition
Mon Dieu, j’ai un très grand regret de vous
avoir offensé parce que vous êtes infiniment
bon et que le péché vous déplaît. Je prends la
ferme résolution, avec le secours de votre
sainte grâce, de ne plus vous offenser et de
faire pénitence.

## regina caelorum
* ! Ajouter aux credits

## alma redemptoris
* ! Ajouter aux credits



# Heures des prieres
* Ressortir le message du serveur (en faire un template / ajouter un content_instance ?)

# Commande Discord ajout de prieres (dans content_instance du coup je pense)

* une commande discord ou tu tappes la prière et ça remplie le json? (modifié)
* mais restreint par utilisateur y not
* et un conseil envoie un preview une fois uqela prière est rentrée
* et met une validation et dans ce cas là tu l'écris en dur
* et un conseil envoie un preview une fois uqela prière est rentrée
* str.replace("dieu","Dieu"), etc ...

# Lier a Framadate
* 1) Est ce qu'ils ont une API ???

# Refrain des intentions de prières lors des vêpres
* Y en a t il plusieurs ?
* Le taper en argument de la fonction ?

# Pour la priere de 20h : proposer une commande :
?priere chant_intro (confiteor)  saint_de_a_semaine[<= comment est il choisi ?] alleluia  Évangile(Mt 5:20-26 le 10 juin)  chant_PU  prières_de_la_journée  silence  angelus_chant  pater  petite_maman_du_ciel(platz)  "que le seigneur ns benisse au nom du pere, fils et st esprit".


# Choix calendrier dans key.json
La disposition des célébrations dans l’année liturgique est réglée par le calendrier.

Ce dernier peut être général (appelé aussi « calendrier général romain ») et ainsi contenir les célébrations destinées à être observées dans tous les pays du monde.
Il peut aussi être particulier à un continent, un pays, un diocèse ou bien encore à une famille religieuse. Il diffère alors légèrement, à de rares dates, au calendrier romain.
Le site de l’AELF vous propose de consulter plusieurs calendriers : le calendrier romain mais aussi ceux propres à l’Afrique du Nord, à la Belgique, au Canada, à la France, au Luxembourg ou à la Suisse.


# Note 1
Demain c'est la saint Barnabé donc ça change pour faire un truc spécial saint Barnabé
3. Le cantique de Zacharie n'est pas à chaque Laudes, celui de Marie (magnificat) n'est pas à chaque vêpres, le but du ptp est de condenser tout en un seul bouquin pas trop épais. Du coup le psaume invitatoire 94 (Venez crions de joie pour le Seigneur, acclamons notre Rocher notre salut) non plus n'est pas mis, d'autant qu'on le chante pas aux laudes mais au 1er office du jour, qui peut être les lectures. J'ai cru entendre qu'on pouvait en prendre d'autres que le 94
PandaE
https://youtu.be/gHxDUgCQHL4
[20:47] ДокторКто: je ne l'avais pas vu, c'est à la fois drôle et gênant 
[20:47] Pablo [modo]: Tu peux trouver l'invitatoire et les cantiques manquant qq part dans le ptp, regarde le sommaire ou sinon l'Office du dimanche, qui est détaché en papier blanc, si tu l'as. Dsl j'ai pas mon ptp sous la main pour 2 semaines

* Psaume en 2 parties : enlever l'antienne (sexte 2022-06-22)

* Priere de la France du bienheuruex Van
* recuperer les messages sur lequel on mettrait une emoticone pour la priere du 20h
* priere du 20h : saint avec vie ; citation et priere
