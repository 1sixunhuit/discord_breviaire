import os
import discord
from discord.ext import commands
import markdown

import globals
import restrictions
from write_logs import *
from formatStrings import *
from bot_user import chat_with_user, add_vote

# ============ Maintainer =============================
class Admin(commands.Cog, name="Restricted"):
    """Commandes bot for maintainer"""
    def __init__(self, bot):
        self.bot = bot
    #endDef

    @commands.command(
        name='bot_help_admin',
        aliases=["help_admin",],
        brief="Aide pour ceux ayant des privilèges particuliers",
        description="Aide pour les personnes ayant des privilèges particuliers (admin, rédacteur, ...)",
        ignore_extra=True,
        hidden=False,
        enabled=True
    )
    async def help_admin(self, ctx):
        allowed, msg = restrictions.allowUsage(ctx, restrict="restrict_add_content")
        if not allowed:
            await ctx.send(":warning: {0}".format(msg))
            return 6
        #endIf

        await ctx.send(
            """```
?ajouter_priere          : Pour ajouter une prière (avec un seul texte ; pour plusieurs, il faut le faire manuellement) ; pas d'arguments car discussion avec l'utilisateur
?reload *                : Reload all modules (maintainer only)
?reload bot_additionnals : Reload only the module "bot_additionnals for exemple (maintainer only)
List of modules          : """+str(globals.bot_modules)+"""```
""")

    #endDef

    @commands.command(
        name='bot_reload',
        aliases=["reload",],
        brief="Recharger un module",
        description="Aide pour les personnes ayant des privilèges particuliers (admin, rédacteur, ...)",
        ignore_extra=True,
        hidden=True,
        enabled=True
    )
    async def _reload(self, ctx, **kwargs):
        """Reloads a module."""

        _kwargs_force="force"
        _kwargs_reload="module"

        # From : https://github.com/Rapptz/RoboDanny/blob/rewrite/cogs/admin.py
        if kwargs.keys():

            if _kwargs_force in kwargs.keys():
                force=kwargs[_kwargs_force]
            else:
                force=False
            #endIf

            if not force:
                allowed, msg = restrictions.allowUsage(ctx, restrict="restrict_add_content")
                if not allowed:
                    await ctx.send(":warning: {0}".format(msg))
                    return 6
                #endIf
            #endIf

            if _kwargs_reload in kwargs.keys():
                module=kwargs[_kwargs_reload]
            #endIf

        else:

            if len(ctx.message.content)<8:
                await ctx.send(":warning: Please specify the module to be restarted, or `all`\n"+F"Available modules : {globals.bot_modules}")
                return 1
            #endIf
            module=ctx.message.content[8:].split(" ")
        #endIf

        if module in ["all","*", ["*",], ["all",]]:
            lmodules=globals.bot_modules
        elif type(module) is list or type(module) is tuple:
            lmodules=module
        elif type(module) is str:
            lmodules=[module,]
        elif not module:
            lmodules=[]
            await ctx.send(":warning: Please specify the module to be restarted, or `all`"+F"Available modules : {globals.bot_modules}")
        else:
            lmodules=[str(module),]
        #endIf

        write_log(
                user=str(ctx.author.name),
                content=F"Reload modules {lmodules}"
        )
        for each_module in lmodules:

            try:
                try:
                    self.bot.unload_extension(each_module)
                except Exception as e:
                    await ctx.send(F'Module {each_module} was not loaded before, but I\'ll try to load it now.')
                    write_log(
                        user=str(ctx.author.name),
                        content=F"Failed to unload  {each_module} : {e}"
                    )
                    print(e)
                    pass
                #endTry

                self.bot.load_extension(each_module)
            except Exception as e:
                await ctx.send('Reload: \N{PISTOL}  {}: {}'.format(type(e).__name__, e))
                write_log(
                    user=str(ctx.author.name),
                    content=F"Failed to load  {each_module} : {e}"
                )
            else:
                await ctx.send('Reload: \N{OK HAND SIGN}  {}'.format(each_module))
            #endTry
        #endFor
    #endDef


    @commands.command(hidden=True)
    async def helloU(self, ctx):
        """Fonction de test"""

        allowed, msg = restrictions.allowUsage(
            ctx,
            restrict="restrict_evangile"
        )

        if not allowed:
            await ctx.send(":warning: {0}".format(msg))
            return 1
        #endIf

        # ctx.author.name
        # ctv.author.mention
        await ctx.send("Hello, @{}  --  {}   :::  {} !! {}".format(ctx.author, ctx.author.mention, ctx.message.guild.name , ctx.message.channel))
    #endDef



    @commands.command(
        name='ajouter_priere',
        aliases=["ajout_priere",],
        brief="Ajouter une prière",
        description="Chat-bot pour ajouter une prière",
        ignore_extra=True,
        hidden=True,
        enabled=True
    )
    async def ajouter_priere(self, ctx):
        """Ajouter une priere via Discord"""

        allowed, msg = restrictions.allowUsage(ctx, restrict="restrict_add_content")
        if not allowed:
            await ctx.send(":warning: {0}".format(msg))
            return 6
        #endIf

        await ctx.send(":small_orange_diamond: Vous pouvez interrompre à tout moment en écrivant \"stop\" :small_orange_diamond:")

        # > Entrer le titre

        await ctx.send(":arrow_forward: Veuillez entrer le **titre** de la prière ")
        msg = await chat_with_user(self.bot, ctx, timer=60.0)

        if msg is None:
            await ctx.send("Annulation")
            return 2
        #endIf

        the_title=str(msg.content)

        # > Entrer le contenu

        await ctx.send(":arrow_forward: Veuillez entrer le **contenu** de la prière")
        msg = await chat_with_user(self.bot, ctx, timer=360.0)

        if msg is None:
            await ctx.send("Annulation")
            return 2
        #endIf

        the_content=str(msg.content)

        # Formattage HTML ou markdown ?

        formatHTML=False
        for e in ["<br/>","<br>","<br />"]:
            formatHTML = formatHTML or e in the_content
        #endFor
        if not formatHTML:

            the_content_html = markdown.markdown(the_content.replace(" *\n","\*<br/>").replace(" * ","\*").replace("\n","<br/>"))
            the_content_html = the_content_html.replace("<em>","<i>").replace("</em>","</i>")
            the_content_html = the_content_html.replace("<strong>","<b>").replace("</strong>","</b>")
            the_content = the_content_html

        #endEif

        # > Entrer la commande a executer

        await ctx.send(F":arrow_forward: Veuillez entrer la **commande a exécuter** pour lancer la prière (**sans** le symbole `{globals.command_prefix}` devant)")
        msg = await chat_with_user(self.bot, ctx, timer=60.0)

        if msg is None:
            await ctx.send("Annulation")
            return 2
        #endIf

        the_command=str(msg.content)

        if the_command[0] == "?" and len(the_command) > 1:
            the_command=the_command[1:]
        elif the_command[0] == "?" and len(the_command) == 1:
            return 3
        #endIf

        # Preparation a la creation du fichier  JSon
        fname=get_valid_filename(the_command)
        the_path=os.path.join(globals.content_additionnals_path, fname+".json")

        while os.path.exists(the_path):
            await ctx.send(F"""Cette commande existe deja, merci d'en choisir une autre""")
            msg = await chat_with_user(self.bot, ctx, timer=180.0)

            if msg is None:
                await ctx.send("Annulation")
                return 2
            #endIf
            the_command=str(msg.content)
            the_fname = get_valid_filename(the_command)
            the_path = os.path.join(globals.content_additionnals_path, the_fname+".json")
        #endWhile


        # > Verification des donnees entrees

        await ctx.send(F":checkered_flag: La commande à lancer sera : `{globals.command_prefix}{the_command}`")

        content_new, _ = post_or_continue("", appendTitleText(the_title, None, the_content,convertHTML=True))
        lcontent=[es for es in splitContent(content_new) if es]
        for es in lcontent:
            await ctx.send(".\n"+es+"\n.")
        #endFor


        # Fin preparation

        await ctx.send(F""":checkered_flag: {ctx.author.mention}, merci de vérifier  et :warning: **d'écrire** `oui`  pour **valider** :warning: la création de la prière
__*Remarque*__ : Vous pourrez demander au mainteneur d'éditer le fichier '{the_path}' à la main si nécessaire""")
        msg = await chat_with_user(self.bot, ctx, timer=180.0)

        if msg is None:
            await ctx.send("Annulation")
            return 2
        #endIf

        verif=str(msg.content)
        if verif.lower() in ["oui","o", "yes", "y", the_command]:
            await ctx.send("Bon ben, c'est à moi de bosser maintenant...")

            # Ecrire le JSon
            nl="\n"
            with open(the_path,"w") as f:
                zone="france"

                f.write(r'{"informations":{'+'\n')
                f.write(r'    "zone":"{'+zone+'}"\n')
                f.write(r'},'+'\n')
                f.write(r' "'+the_command+r'":{"titre":"'+the_title+'",\n')
                f.write(r'     "texte":"'+the_content+'"\n')
                f.write(r'	   },'+'\n')
                f.write(r' "deroulement":['+'\n')
                f.write(r'     {"type":2,"key":"'+the_command+r'"}'+'\n')
                f.write(r'     ]'+'\n')
                f.write(r'}')
            #endWidth
            with open("bot_additionnals.py","a") as f:
                f.write(r'    @commands.command()'+'\n')
                f.write(r'    async def '+the_command+r'(self, ctx):'+'\n')
                f.write(r'        """'+the_title+r'"""'+'\n')
                f.write(r'        await main_bot')
                f.write(r'(ctx=ctx, priere="'+the_command+r'", ')
                f.write(r'restrict=globals.restrict_additionnals) '+'\n')
                #f.write(r' directory="'+globals.content_additionnal_path+r'")'+'\n')
                f.write(r'        return 0'+'\n')
                f.write(r'    #endDef'+'\n\n')
            #endWidth

            maintainer_id = await self.bot.fetch_user(globals.maintainer_id)

            await ctx.send(F"""Fait ! Le module va redémarrer automatiquement et sera de nouveau disponible dans 1 seconde :watch:"""+
                           "\nMerci d'avoir utilisé cette fonction. Si vous désirez passer cet ajout dans le compendium, veuillez contacter"+F"{maintainer_id.mention}")
        else:
            await ctx.send("Annulation")
        #endIf



        await self._reload(ctx, force=True, module="bot_additionnals")
    #endDef
#endClass


def setup(bot):
    bot.add_cog(Admin(bot))
#endDef
