import re
from bs4 import BeautifulSoup, Tag

LENMAX=1999

def get_valid_filename(s):
    s = str(s).strip().replace(' ', '_').replace('\xa0', '_')
    return re.sub(r'(?u)[^-\w.]', '', s)
#endDef

def post_or_continue(content_old, addition):
    return (addition, len(content_old)+len(addition)>LENMAX)
#endDef

# @brief : Nettoyage fonction
# Todo : renommer : html2discord
def formatMsg(content, resplit=False, **kwargs):

    if content is None or content == "_ _":
        return None
    #endIf

    # Enlever mots en majuscule
    content=" ".join([
        word.title() if (
            (word == word.upper())and
            (not word in ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII','VIII', 'IX', 'X'])
        )
        else word
        for word in content.split(" ")
    ])

    if resplit:
        content=content.replace(".",".<br/>")
        content=content.replace(";",";<br/>")
        content=content.replace(":",":<br/>")
    #endIf

    # Simplifications
    _replaceRVerset="/®®®/"
    _replaceVVerset="/ßßß/"
    content=content.replace('<span class="verse_number">R/</span>', "R/ ")
    content=content.replace('<span class="verse_number">V/</span>', "V/ ")
    content=content.replace('<span class="verse_number">R/', _replaceRVerset) # pour eviter les soucis, avec <span>R/ 01</span> par exemple ds les psaumes
    content=content.replace('<span class="verse_number">V/', _replaceVVerset)
    content=content.replace("<span class=\"verse_number\">","<b>")
    content=content.replace('<span class="verse_number">','<b>')
    content=content.replace("</span>"," </b>")
    content=content.replace("<span>","<b>")
    content=content.replace("<strong>","<b>")
    content=content.replace("</strong>","</b>")
    content=content.replace("<em>","<i>")
    content=content.replace("</em>","</i>")

    content=content.replace("<br>","<br/>")
    content=content.replace("<br />","<br/>")
    #content=content.replace("\n\n\n","<br/><br/>")
    #content=content.replace("\n\n\n\n","<br/><br/>")
    #content=content.replace("\n\n","<br/><br/>")
    content=content.replace("\n","")
    content=content.replace("*","<b>&star;</b>")
    content=content.replace("+","<b>+</b>")
    content=content.replace("_","&lowbar;")

    # Ajout gras la ou il n'y en a pas
    content=content.replace("(R/)","<b>(R/)</b>")
    content=content.replace("R/","<b>R/</b>")
    content=content.replace("V/","<b>V/</b>")

    content=content.replace(_replaceRVerset,"<b>R/ ") # la fermeture de la balise devrait aller apres le </span> quand il y a des versets ;)
    content=content.replace(_replaceVVerset,"<b>V/ ")

    # Doublons (pre)
    #content=content.replace("<span><span>","").replace("</span></span>","")



    # Doublons
    def removeDuplicates(content, tag="br"):

        soup = BeautifulSoup(content)

        for br in soup.find_all(tag):
            while isinstance(br.next_sibling, Tag) and br.next_sibling.name == tag:
                br.next_sibling.extract()
            #endWhile
        #endFor
        return str(soup)
    #endDef

    removeDuplicates(content, tag="b")
    removeDuplicates(content, tag="i")
    removeDuplicates(content, tag="u")


    content="<br/>".join([e.strip() for e in content.split("<br/>")]).strip()


    return content

#endDef


def html2markdown(content, displayMarkdown=False, **kwargs):

    if content is None:
        return None
    #endIf

    # Accents
    # https://www.journaldunet.com/solutions/dsi/1195751-accents-caracteres-speciaux-html/
    content=content.replace("&agrave;","à")
    content=content.replace("&Agrave;","À")
    content=content.replace("&acirc;","â")
    content=content.replace("&Acirc;","Â")
    content=content.replace("&eacute;","é")
    content=content.replace("&Eacute;","É")
    content=content.replace("&egrave;","è")
    content=content.replace("&Egrave;","È")
    content=content.replace("&ecirc;","ê")
    content=content.replace("&Ecirc;","Ê")
    content=content.replace("&euml;","ë")
    content=content.replace("&Euml;","Ë")
    content=content.replace("&aelig;","æ")
    content=content.replace("&AEmig;","Æ")
    content=content.replace("&icirc;","î")
    content=content.replace("&Icirc;","Î")
    content=content.replace("&iuml;","ï")
    content=content.replace("&Iuml;","Ï")
    content=content.replace("&ocirc","ô")
    content=content.replace("&Ocirc","Ô")
    content=content.replace("&ograve","ù")
    content=content.replace("&Ograve","Ù")

    # Symboles
    content=content.replace("&nbsp;","\xa0")
    content=content.replace("&rsquo;","'")
    content=content.replace("&laquo;","«")
    content=content.replace("&raquo;","»")

    if(displayMarkdown):
        content=content.replace("&gt;","\>")
        content=content.replace("&lt;","<")
        content=content.replace("&star;","\\\\\*")
        content=content.replace("&lowbar;","\\\\\_")
    else:
        content=content.replace("&gt;",">")
        content=content.replace("&lt;","<")
        content=content.replace("&star;","\*")
        content=content.replace("&lowbar;","\_")
    #endIf

    # Markdown
    content=content.replace("<p>","")
    content=content.replace("</p>","\n")
    content=content.replace("<br/>","\n")

    if(displayMarkdown):
        content=content.replace("<u>","\_\_")
        content=content.replace("</u>","\_\_")
        content=content.replace("<i>","\*")
        content=content.replace("</i>","\*")
        content=content.replace("<b>","\*\*")
        content=content.replace("</b>","\*\*")
    else:
        content=content.replace("<u>","__")
        content=content.replace("</u>","__")
        content=content.replace("<i>","*")
        content=content.replace("</i>","*")
        content=content.replace("<b>","**")
        content=content.replace("</b>","**")
    #endIf

    return content
#endDef


def appendTitleText(
        title=None, ref=None,
        content=None,
        subtitle=None,
        author=None, editor=None,
        convertHTML=True,include=False,
        **kwargs):
    #TODO : renomer "content" en "text"

    if content is None:
        return ""
    elif content == "":
        return ""
    #endIf
    out="<br/>"

    def part2formated(arg, template="", template_key="ARG"):
        listEmpty = ["", " ", "  ","_ _", "__  __", "\xa0"]

        if arg is None or not arg.strip() or arg in listEmpty:
            arg_formated = ""
            arg = None
        else:
            arg_tmp = formatMsg(arg)
            arg_formated = template.replace(template_key, arg_tmp )
        #endIf

        return arg_formated, arg
    #endDef

    ref_formated, ref = part2formated(ref, template=" (<i>REF</i>)", template_key="REF")
    title_formated, title = part2formated(title, template="<b>TTL</b>", template_key="TTL")
    subtitle_formated, subtitle = part2formated(subtitle, template="<i>STTL</i>)", template_key="STTL")
    author_formated, author = part2formated(author, template="auteur: <i>AUT</i>", template_key="AUT")
    editor_formated, editor = part2formated(editor, template="éditeur: <i>ED</i>", template_key="ED")

    if title_formated or ref_formated:
        title_complet="{ttl}{ref}".format(ttl=title_formated, ref=ref_formated)
    else:
        title_complet = None
    #endIf

    if title_complet is not None:
        out +="<br/>{lft}{ttl}{rgt}".format(
            lft="&star;&gt;",
            rgt="&lt;&star;",
            ttl=title_complet.center(45, " ")
        )
    #endIf

    if subtitle_formated :
        out+="<br/>{}".format(subtitle_formated)
    #endIf
    if author_formated and editor_formated :
        out+=F"<br/>{author_formated.capitalize()}, {editor_formated}"
    elif author_formated :
        out+=F"<br/>{author_formated.capitalize()}"
    elif editor_formated :
        out+=F"<br/>{editor_formated.capitalize()}"
    #endIf

    out+="<br/><br/>{txt}<br/>".format(txt = formatMsg(content, **kwargs))

    if convertHTML:
        return html2markdown(out, **kwargs)
    else:
        return out
    #endIf

    sys.stderr.write("formatString 2 : UNEXPECTED !!!\n")

#endDef


# @brief Split
#             - default : sur les retours a la ligne
#             - si ligne trop longue : sur la ponctuation . ! ? : ;
#             - si ligne encore trop longue : sur la ponctuation ,
#             - si ligne encore trop longue : sur les espace
#             Et on s'arrete la : le mot "anticonstitutionnellement" faisant moins de 2000 caracteres

#ToDo : intentation, rendre lisible, optimiser
def splitContent(content, LENMAX=1999):

        ltmp=[e+"\n" for e in content.split("\n")]

        i=0
        lcontent = ["",]
        for e in ltmp:
            if len(e) >= LENMAX:
                ls = re.findall(r'(?:\d[.\!\?\:\;]|[^.\!\?\:\;])*(?:[.\!\?\:\;]|$)', e)
                j=0
                le = ["",]
                for f in ls:
                    if not f:
                        continue
                    #endIf
                    k = None
                    if len(f) >= LENMAX:
                        lff = re.findall(r'(?:\d[,]|[^,])*(?:[,]|$)', f)
                        lf = ["",]
                        k = 0
                        l = None
                        for g in lff:
                            if len(g) >= LENMAX:
                                lgg=[h+" " for h in g.split(" ")]
                                l = 0
                                lg = ["",]
                                for h in lgg:
                                    if len(lg[l]) +len(h) >= LENMAX:
                                        l += 1
                                        lg += ["",]
                                    #endIf
                                    lg[l] += h
                                #endFor
                                lf += lg
                                k += l + 1

                            elif len(lf[j]) + len(g) >= LENMAX:
                                k  += 1
                                lf += ["",]
                            #endIf
                            if l is None:
                                lf[k] += g
                            #endIf
                        #endFor
                    elif len(le[j]) + len(f) >= LENMAX:
                        j  += 1
                        le += ["",]
                    #endIf

                    if k is None:
                        le[j] += f
                    else:
                        j += k+1
                        le +=lf
                    #endFor
                #endFor

                i+=j+1
                lcontent+=le

            else:
                if len(lcontent[i])+len(e) >= LENMAX:
                    i += 1
                    lcontent += ["",]
                #endIf
                lcontent[i] += e
            #endIf
        #endFor

        return list([e for e in lcontent if e not in ["\n", ""]])
    #endDef
