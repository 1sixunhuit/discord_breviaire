from discord.ext import commands

import globals
import restrictions
from write_logs import *
from publish import *

class Compendium(
        commands.Cog,
        name="Prières",):

    def __init__(self, bot):
        self.bot = bot
    #endDef


    @commands.command(
        name="angelus_chant",
        aliases=["angélus_chanté","chant_angelus", "chant_angélus","angelus_soir"],
        description="Chant de l'Angélus",
        ignore_extra=True,
    )
    async def angelus_chant(self, ctx):
        """Angélus chanté"""
        await main_bot(ctx=ctx,priere="angelus_chant", restrict=globals.restrict_compendium)
        return 0
    #endDef


    @commands.command(
        name="angelus",
        aliases=["angélus",],
        description="Angélus"
    )
    async def angelus(self, ctx):
        """Angélus"""

        if(
                ("chant" in ctx.message.content) or
                ("chante" in ctx.message.content) or
                ("chanté" in ctx.message.content) or
                ("soir" in ctx.message.content)
        ):

            await self.angelus_chant(ctx)
            return 0

        else:

            await main_bot(ctx=ctx,priere="angelus", restrict=globals.restrict_compendium)
            return 0

        #endIf

    #endDef


    @commands.command(
        name="regina_caeli",
        aliases=["reginacaeli","regina_cæeli","reginacæeli","regina"],
        description="Regina Cæli",
        ignore_extra=True,
        )
    async def regina_caeli(self, ctx):
        """Regina cæeli"""

        #await ctx.message.add_reaction("\U0001F47C")
        # https://www.fileformat.info/info/unicode/char/search.htm?q=angel&preview=entity

        await main_bot(ctx=ctx,priere="regina_caeli", restrict=globals.restrict_compendium)
        return 0
    #endDef


    @commands.command(
        name="salve_regina",
        aliases=["salveregina","salve"],
        description="Salve Regina",
        ignore_extra=True,
    )
    async def salve_regina(self, ctx):
        """Salve regina"""
        await main_bot(ctx=ctx,priere="salve_regina", restrict=globals.restrict_compendium)
        return 0
    #endDef


    @commands.command(
        name="priere_pour_yaedia",
        aliases=["priere_yaedia","priereyaedia","yaedia"],
        description="Prière pour Yaedia",
    )
    async def yaedia(self, ctx):
        """Prière pour Yaedia"""

        await main_bot(ctx=ctx,
                       priere="pour_yaedia",
                       light=False,
                       force_format_short_version=only_short_pray(ctx.message.content),
                       restrict=globals.restrict_compendium,
                       personnalisation="yaedia")
        return 0
    #endDef

    @commands.command(
        name="priere_defunts",
        aliases=["prieredefunts","priere_defunt","prieredefunt","defunts", "defunt", "priere_tous", "prieretous"],
        description="Prière pour les défunts (psaume 129)",
    )
    async def defunts(self, ctx):
        """Prière pour les défunts"""

        await main_bot(ctx=ctx,
                       priere="priere_defunts",
                       restrict=globals.restrict_compendium
                       )
        return 0
    #endDef


    @commands.command(
        name="avant_la_fin_de_la_lumiere",
        aliases=["avant_la_fin_de_la_lumière",
                 "avantlafindelalumiere",
                 "avantlafindelalumière",
                 "lumiere","lumière"],
        description="Hymne *Avant la fin de la lumière*",
        ignore_extra=True,
    )
    async def avant_la_fin_de_la_lumiere(self, ctx):
        """Hymne *Avant la fin de la lumière*"""
        await main_bot(ctx=ctx,priere="hymne_avant_la_fin_de_la_lumiere",
                       restrict=globals.restrict_compendium)
        return 0
    #endDef


    @commands.command(
        name="ave_maria",
        aliases=["ave", "avemaria", "jvsm", "je_vous_salue_marie"],
        description="Je vous salue Marie (Ave Maria)",
        ignore_extra=True,
    )
    async def ave_maria(self, ctx):
        """Je vous salue Marie"""
        await main_bot(ctx=ctx, priere="ave_maria", restrict=globals.restrict_compendium)
        return 0
    #endDef


    @commands.command(
        name="pater_noster",
        aliases=["pater","notre_père","notre_pere"],
        description="Notre Père (Pater Noster))",
        ignore_extra=True,
    )
    async def pater_noster(self, ctx):
        """Notre Père"""
        await main_bot(ctx=ctx, priere="pater_noster", restrict=globals.restrict_compendium)
        return 0
    #endDef


    @commands.command(
        name="credo_apotres",
        aliases=["credo_apôtres",
                 "symbole_des_apotres","symbole_apotres","apotres",
                 "symbole_des_apôtres","symbole_apôtres","apôtres",
                 ],
        description="Credo : symbole des apôtres",
        ignore_extra=True,
    )
    async def credo_apotres(self, ctx):
        """Credo : Symbole des apôtres"""
        await main_bot(ctx=ctx, priere="credo_apotres", restrict=globals.restrict_compendium)
        return 0
    #endDef


    @commands.command(
        name="credo_nicee",
        aliases=["credo","nicee","credo_nicee_constantinople"],
        description="Credo de Nicée-Constantinople"
    )
    async def credo_nicee(self, ctx):
        """Credo de Nicée-Constantinople"""

        if(
                ("symbole" in ctx.message.content) or
                ("apotre" in ctx.message.content) or
                ("apôtre" in ctx.message.content) or
                ("rapide" in ctx.message.content) or
                ("court" in ctx.message.content)
        ):

            await self.credo_apotres(ctx)
            return 0

        else:

            await main_bot(ctx=ctx, priere="credo_nicee", restrict=globals.restrict_compendium)
            return 0

        #endIf

        return 0
    #endDef


    @commands.command(
        name="contrition_long",
        aliases=["acte_contrition_long","acte_de_contrition_long"],
        description="Acte de contrition (long)",
        ignore_extra=True,
    )
    async def contrition_long(self, ctx):
        """Acte de contrition (long)"""
        await main_bot(ctx=ctx, priere="contrition_long", restrict=globals.restrict_compendium)
        return 0
    #endDef


    @commands.command(
        name="contrition_court",
        aliases=["contrition", "acte_contrition","acte_de_contrition",
                 "acte_contrition_court","acte_de_contrition_court",
                 ],
        description="Acte de contrition (version courte 'classique')",
    )
    async def contrition_court(self, ctx):
        """Acte de contrition"""

        if(
            ("long" in ctx.message.content)
        ):

            await self.contrition_long(ctx)
            return 0

        else:

            await main_bot(ctx=ctx, priere="contrition_court", restrict=globals.restrict_compendium)
            return 0

        #endIf
    #endDef


    @commands.command(
        name="dizaine",
        aliases=["dizainier",
                 ],
        description="Dizaine : 1 Pater + 10 Ave",
        ignore_extra=True,
    )
    async def dizaine(self, ctx):
        """Dizaine (1 Pater + 10 Ave)"""
        await main_bot(ctx=ctx, priere="dizaine", restrict=globals.restrict_compendium)
        return 0
    #endDef

    # @commands.command(
    #     name="chapelet",
    #     # aliases=["",
    #     #          ],
    #     description="Chapelet",
    #     ignore_extra=True,
    # )
    # async def chapelet(self, ctx):
    #     """Chapelet"""
    #     await main_bot(ctx=ctx, priere="chapelet", restrict=globals.restrict_compendium)
    #     return 0
    # #endDef
#endClass


def setup(bot):
    bot.add_cog(Compendium(bot))
#endDef

