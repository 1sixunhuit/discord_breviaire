import sys, os
import re
import requests, json

from aelfRequest import *
from formatStrings import *
from write_logs import *

import globals




# ============================================

def datas2message(datas,
                  LENMAX=1999,
                  confiteor=None,
                  convertHTML=True,
                  force_format_short_version=False,
                  debug=None,
                  **kwargs
                  ):

    if debug is None:
        debug=globals.debug
    #endIf

    # Confiteor : None, missel (defaut), dominicain
    # convertHTML : appliquer conversion rmatage pour enlever le HTML

    # Init :
    content_office = ["",]  # OUT : tableau de moins de LENMAX caracteres
    content=""

    # TODO : checker si toutes les cles seront appelees
    # for progress_item in datas_description

    def goToNext(content_office, content):
        content_office += [es for es in splitContent(content) if es]
        content=""
        return content_office, content
    #endDef

    for datas_item in datas:

        if type(datas_item) in (list, tuple):
            sys.stderr.write("Unexpected error 1 (datas2message)\n{}\n\n".format(str(datas_item)))
            exit(8)
        #endIf

        if force_format_short_version and "format_priere" in datas_item.keys():

            if (not datas_item["format_priere"] ):
                continue
            #endIf

        #endIf

        if datas_item == {} :
            content_office, content = goToNext(content_office, content)
            continue
        elif datas_item["key"] is None or datas_item["new_section"] :
            #todo : prendre en charge "new_section" correctement
            content_office, content = goToNext(content_office, content)
            continue
        #endIF

        postNow=False

        the_title=None
        the_content=None
        the_reference=None
        the_author=None
        the_editor=None

        def keyInDico(dico, key):
            return dico[key] if key in dico.keys() else None
        #endDef

        try:
            addition = None

            # Todo : factoriser tout ca
            the_title = keyInDico(datas_item, "titre")
            the_content = keyInDico(datas_item, "texte")
            the_reference = keyInDico(datas_item, "reference")

            the_author = keyInDico(datas_item,"auteur")
            the_editor = keyInDico(datas_item,"editeur")

        except Exception as e:
            sys.stderr.write("ERROR 1\n")
            sys.stderr.write("ERROR : "+str(e)+"\n")
            sys.stderr.write("DATAS : "+str(datas_item)+"\n")


            write_log(
                user="jsonfiles2datas.py",
                content="Error (jsondatas2message) 2 : \n{}".format(str(e))
            )
            if debug:
                exit(21)
            else:
                pass
            #endIf
        #endTry

        try:
            if datas_item is not None :
                if ("key" in datas_item.keys()) :
                    if datas_item["key"] is not None:
                        resplit =  ("antienne" in datas_item["key"])
                    #endIf
                #endIF
            #endIf

            content_to_add = appendTitleText(
                title=the_title, ref=the_reference, content=the_content,
                author=the_author, editor=the_editor,
                convertHTML=convertHTML,
                resplit=resplit,
                **kwargs
            )

            content_new, postNow = post_or_continue(content, content_to_add)

            if postNow:

                content_office += [es for es in splitContent(content) if es]
                content = content_new

            else:
                content += content_new
            #endIf

        except Exception as e:
            sys.stderr.write("ERROR 2\n")
            sys.stderr.write(str(e)+"\n")
            sys.stderr.write(str(datas_item)+"\n")

            write_log(
                user="jsonfiles2datas.py",
                content="Error (jsondatas2message) 20 : \n{}".format(str(e))
            )
            if debug:
                exit(210)
            else:
                pass
            #endIf
        #endTry

    #endFor

    try:

        content_office += [es for es in splitContent(content) if es]

    except Exception as e:
        sys.stderr.write("ERROR 3\n")
        sys.stderr.write(str(e)+"\n")
        sys.stderr.write(str(datas_json[key])+"\n")
        write_log(
            user="jsonfiles2datas.py",
            content="Error (jsondatas2message) 3 : \n{}".format(str(e))
        )

    #endTry

    del content_office[0] # car on a init la liste avec ""

    return content_office






def evangile_request(office_name,
                     LENMAX=1999,
                     **kwargs):

    office = aelf2datas("messes")[0]["lectures"]

    try:
        for eachLecture in office:
            if eachLecture["type"] !="evangile":
                continue
            #endIf

            the_intro   = eachLecture["intro_lue"]
            the_ref     = eachLecture["ref"]
            the_content = eachLecture["contenu"]

            the_content = "<p>".join(the_content.split("<p>")[:-1])  #ex : replace("– Acclamons la Parole de Dieu.</p>","")

            if the_content != "None":
                content_new, postNow = post_or_continue("", appendTitleText(
                    title=the_intro, ref=the_ref, content=the_content,
                    author=None, editor="AELF",
                    **kwargs))
            #endIf

    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        sys.stderr.write(str(office)+"\n")
        write_log(
            user="jsonfiles2datas.py",
            content="Error (evangile_request) : \n{}".format(str(e))
        )
        pass
    #endTry


    content_office = [es for es in splitContent(content_new) if es]

    return content_office


if __name__=="__main__":
    from collect_datas import *

    debug = True


    print("* ** *** **** *** ** *")
    print("content offices")
    # getContentOffice :
    office_name = "complies"
    #office_name = "lectures"
    #office_name = "laudes"
    datas_description = readJson('./content_office/{}.json'.format(office_name), key="deroulement")

    office = aelf2datas(office_name=office_name, the_day="today")
    out = collectDatas(name=office_name, progress_datas=datas_description, content_datas=office, debug=True)

    print("* ** *** **** *** ** *")
    print("content compendium")

    name="chapelet"
    name="pour_yaedia"
    datas, personnalisation = json2datas(name, debug=debug, personnalisation_key="yaedia")
    datas = personnalisation_datas(datas, personnalisation)
    datas = add_doxologie(datas)

    print("todo : add_doxo : debug")
    content=datas2message(datas, debug=debug,force_format_short_version=True)


    # ################

    print("vv --------------------------------- vv ---------------------------------- vv")
    #for e in content:
    #    print(e)
    #    print("-------------------------------------------")
