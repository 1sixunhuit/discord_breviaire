import sys, os
import re

from aelfRequest import *
from formatStrings import *
from write_logs import *
from jsonReadFile import jsonFile2RawDatas, readJson

import globals


def getMystere(**kwargs):

    mystere = None

    if kwargs is not None:
        if "mystere" in kwargs.keys():
            mystere = kwargs["mystere"]
        #endIf
    #endIf

    if mystere is None:
        the_day, the_time, mystere = association_jour_mystere(
            date="demain"
        )
    #endIf

    return mystere
#endDef


def association_jour_mystere(date=None, debug=False):

    info_aelf = aelf2datas("informations",the_day=date)

    time=info_aelf["temps_liturgique"]
    day=info_aelf["jour"]


    if day in ["lundi", "samedi"]:
        mystere="joyeux"
    elif day in ["mardi", "vendredi"]:
        mystere="douloureux"
    elif day in ["mercredi",]:
        mystere="glorieux"
    elif day in ["jeudi",]:
        mystere="lumineux"
    elif day in ["dimanche",]:
        if time in ["careme"]:
            mystere="douloureux"
        elif time in ["avant",]:
            mystere="joyeux"
        else:
            mystere="glorieux"
        #endIf
    else:
        sys.stderr.write("Error (associtation_jour_mystere) : unexpected")
        if debug:
            exit(30)
            #endIf
        #endIf
    #endIf

    return day, time, mystere


def json2datas(name=None,
               datas=None,
               convertHTML=True,
               include=True, light=True,
               personnalisation_key=None,
               debug=None,
               **kwargs):
    # convertHTML : Convertir le HTML
    # include : a inclure dans un message existant, ou s'en servir independamment ?
    # ligth : structure legere (office=content), par comparaison a la structure complete d'AELF

    if debug is None :
        debug=globals.debug
    #endIf

    if datas is None and name is not None:
        content = jsonFile2RawDatas(name, debug=debug, **kwargs)
    elif datas is not None:
        content=datas
    else:
        sys.stderr.write("json2datas : not implemented yet !\n")
    #endIf

    _key_perso = "personnalisation"
    _key_key = "key"

    try:

        id_personnalisation = -1
        i = 0
        datas_perso = None

        if content is None :
            sys.stderr.write("**Warning : content for {} is None in collect_datas\n".format(name))
        #endIf

        if _key_perso in content.keys() and personnalisation_key is not None:

            for elt in content[_key_perso]:

                if id_personnalisation == -1 and elt[_key_key]==personnalisation_key :

                    id_personnalisation = i
                    datas_perso = content[_key_perso][id_personnalisation]
                elif elt[_key_key]==personnalisation_key :

                    sys.stderr.write("Error (jsonfiles2datas.py) : contenuCompendium 00 : {}".format(str(i), str(id_personnalisation)))
                    write_log(
                        user="Internal",
                        content="Internal error (contenuCompendium) : key for personnalisation found twice"
                    )

                #endIf

                i = i + 1

            #endFor


            if id_personnalisation == -1:

                personnalisation_key = None
                sys.stderr.write("Error (jsonfiles2datas.py) : contenuCompendium 01 : key for personnalisation not found {}".format(personnalisation_key))
                write_log(
                    user="Internal",
                    content="Internal error (contenuCompendium) : key for personnalisation not found"
                )

            #endIf

        #endIf

        #if type(content) is dict:

        #if name in list(content.keys()) and light:
        #    light = (type(content[name]) is str)
        ##endIf

        datas = collectDatas(
            name=name,
            progress_datas=content["deroulement"],
            content_datas=content[name] if name in content.keys() else content,
            convertHTML=convertHTML,
            debug=debug,
            **kwargs
        )

    except ValueError as e: #include json.decoder.JSONDecodeError
        sys.stderr.write(name+"\n")
        sys.stderr.write(the_path+"\n")
        sys.stderr.write(str(e)+"\n")
        write_log(
            user="jsonfiles2datas.py",
            content="Error (contenuCompendium) : \n{}".format(str(e))
        )
        if(debug):
            sys.stderr.write("collect_datas 0708 - 3\n")
            exit(2)
        else:
            return "", None
        #endIf
    except Exception as e:
        sys.stderr.write(str(e)+"\n")
        write_log(
            user="jsonfiles2datas.py",
            content="Error (contenuCompendium) : \n{}".format(str(e))
        )
        if(debug):
            sys.stderr.write("collect_datas 0708 - 4\n")
            exit(2)
        else:
            return "", None
        #endIf
    #endTry

    return datas, datas_perso

#endDef

def contenuCompendium(name,
                      convertHTML=True,
                      include=True, light=True,
                      personnalisation=None,
                      debug=False,
                      **kwargs):
    out, _ = json2datas(name,
                      convertHTML=convertHTML,
                      include=include, light=light,
                      personnalisation_key=personnalisation,
                      debug=debug,
                      **kwargs)
    return out
#endDef


def getContentMesse(content_name, **kwargs):
    datas_description = readJson('./content_office/{}.json'.format(content_name), key="messe")

    if content_name=="evangile":
        return evangile_request(office_name="messes", datas_description=datas_description, **kwargs)
    else:
        sys.stderr.write("Error (getContentMesse) : {}  not implemented".format(content_name))
        write_log(
            user="jsonfiles2datas.py",
            content="Error (getContentMesse) : {}  not implemented".format(content_name)
        )
        return 2
    #endIf
#endDef


def getContentOffice(office_name, **kwargs):

    datas_description = readJson('./content_office/{}.json'.format(office_name), key="deroulement")

    office = aelf2datas(office_name=office_name, the_day="today")

    return jsondatas2message(office_name=office_name, datas_description=datas_description, datas_json=office, **kwargs)
#endDef


def getContenuSupplementaire(name, **kwargs):
    sys.stderr.write("Depreciated : getContenuSupplementaire !\n")
    return contenuCompendium(name, convertHTML=True,include=False, **kwargs)
#endDef


def titleOrRef_in_item(progress_item, content_datas,
                       key_progress="title", key_content=None,
                       progress_item_key="key",
                       content_over_progress=False,
                       debug=False):

    # si key_content ou key_progress est null : prendre l'autre valeur

    if key_content is None:
        key_content = key_progress
    #endIf
    if key_progress is None :
        key_progress = key_content
    #endIf
    if key_progress is None and key_content is None :
        sys.stderr.write("Error a :  {}\n".format(key_progress))
        return "None titleOrRef : -1 "+key_progress if debug else None
    #endIf


    title_content = None
    title_progress = None

    if type(progress_item) is str:
        sys.stderr.write("unexpected 0708 - 100 !"+progress_item+"\n")
        if(debug):
            exit(3)
        #endif
    elif key_progress in progress_item.keys():
        # le titre est present dans le deroulement
        # "deroulement":["title": "xxx" ,...]
        title_progress = progress_item[key_progress]
    #endIf

    if type(content_datas) is str:
        # "nom_fichier:"bla bla bla"
        # pas de titre ici
        pass
    elif progress_item_key in progress_item.keys():

        if progress_item[progress_item_key] in content_datas.keys():
            # dans le contenu :
            # "nom_fichier":{"titre":"xxx",...}

            try:
                if type(content_datas[progress_item[progress_item_key]]) is str:
                    pass
                elif content_datas[progress_item[progress_item_key]] is None:
                    pass
                elif not (content_datas[progress_item[progress_item_key]]) :
                    pass

                elif key_content in content_datas[progress_item[progress_item_key]].keys():

                    title_content = content_datas[progress_item[progress_item_key]][key_content]

                else:

                    pass

                #endIf

            except Exception as e:
                sys.stderr.write(str(e)+"\n")
                if(debug):
                    sys.stderr.write("collect_datas 0708 - 30\n")
                    exit(2)
                #endIf
            #endTry
        elif key_content in content_datas.keys():
            try:
                title_content = content_datas[key_content]
            except Exception as e:
                sys.stderr.write(str(e)+"\n")
                if(debug):
                    sys.stderr.write("collect_datas 0708 - 31\n")
                    exit(2)
                #endIf
            #endTry
        #endIf
    elif key_content in content_datas.keys():

        # "nom_fichier:{"titre": "xxx", ...}
        try:
            title_content = content_datas[key_content]
        except Exception as e:
            sys.stderr.write(str(e)+"\n")
            if(debug):
                sys.stderr.write("collect_datas 0708 - 31\n")
                exit(2)
            #endIf
        #endTry
    #endIf

    return title_progress, title_content

#endDef

def text_in_item(
        progress_item,
        content_datas,
        key_progress="content",
        key_content="texte",
        progress_item_key="key",
        debug=False
):
    _, text = titleOrRef_in_item(
        progress_item,
        content_datas,
        key_progress=key_progress,
        key_content=key_content,
        progress_item_key=progress_item_key,
        debug=debug)

    if not text is None and not "None" in text:
        return text
    #endIf

    if key_progress in progress_item.keys():
        return progress_item[key_progress]
    elif type(content_datas) is str:
        return content_datas
    elif key_content in content_datas.keys():
        return content_datas[key_content]
    elif progress_item_key in content_datas.keys():
        try:
            if type(content_datas[progress_item_key]) is str:
                return content_datas[progress_item_key]
            elif content_datas[progress_item_key] is None:
                return "None text : -4" if debug else None
            elif not content_datas[progress_item_key] :
                return "None text : -5" if debug else None
            elif key_content in content_datas[progress_item_key].keys():
                return content_datas[progress_item_key][key_content]
            else:
                return "None text : -3" if debug else None
            #endIf
        except Exception as e:
            sys.stderr.write(str(e)+"\n")
            if(debug):
                sys.stderr.write("collect_datas 0708 - 40\n")
                exit(2)
            #endIf
        #endTry
    else:
        return "None text : -1" if debug else None
    #endIf

    return "None text : -2" if debug else None


def personnalisation_datas(datas, personnalisation, key_text="texte", key_title="titre"):

    if personnalisation is None or not personnalisation:
        return datas
    elif type(personnalisation) in (list, tuple):
        words_to_replace = None
    elif type(personnalisation) is dict:
        words_to_replace = personnalisation
    else:
        sys.stderr.write("personnalisation_datas  : unexpected !!!!\n")
    #endIf

    datas_out = [{}]

    for datas_item in datas:

        datas_out[-1] = {}

        for key_item_datas, value_item_datas in datas_item.items():

            key_item_datas_out = key_item_datas
            value_item_datas_out = value_item_datas

            for key, value in words_to_replace.items():

                existing_word="${}".format(key)
                new_word=str(value)

                if key_item_datas is None:
                    key_item_datas_out = key_item_datas_out
                elif type(key_item_datas) is str :
                    key_item_datas_out = key_item_datas_out.replace(existing_word, new_word)
                else:
                    key_item_datas_out = key_item_datas_out
                #endIf
                if value_item_datas is None:
                    value_item_datas_out = value_item_datas_out
                elif type(value_item_datas) is str :
                    value_item_datas_out = value_item_datas_out.replace(existing_word, new_word)
                else:
                    value_item_datas_out = value_item_datas_out
                #endIf

                datas_out[-1][key_item_datas_out] = value_item_datas_out

            #endFor
        #endFor
        datas_out += [{}]
    #endFor

    return [e for e in datas_out if e]
#endDef


def add_doxologie(datas,
                  doxologie_name="doxologie_court",
                  add_doxo_after = ["psaume", "cantique"],
                  title_item_content="titre",
                  content_item_content="texte",
                  key_item_progress="key",
                  key_force_skip_doxo = "force_skip_doxo"
                  ):


    # ============== Ajouter les doxos ================= #
    list_pos_doxo = []
    len_datas = len(datas)
    append_doxo = [ False for e in range(len_datas)]


    #if  authorize_recursive :  # Ici, toute les recursivites globales
    _content_doxo,_ = json2datas(doxologie_name, authorize_recursive=False) # eviter les appels multiples inutiles

    for i in range(len_datas):

        if datas[i][key_item_progress] is None:
            continue
        elif "mariale" in datas[i][key_item_progress].lower():
            continue
        #endIf

        for elt in add_doxo_after:
            if type(datas[i][title_item_content]) is str:
                try:

                    append_doxo[i] = ( append_doxo[i] or
                                       (
                                           elt.lower() in datas[i][key_item_progress].lower() and
                                           datas[i][content_item_content] is not None
                                       )
                                      )
                except Exception as e:
                    sys.stderr.write(str(e)+"\n")
                    sys.stderr.write(str( datas[i][key_item_progress]))
                    exit(4)
                #endTry
                if key_force_skip_doxo in datas[i].keys():
                    if datas[i][key_force_skip_doxo]:
                        append_doxo[i] = False
                    #endIf
                #endIf
            #endIf

        #endFor

        if ( append_doxo[i] and
             i+1 < len_datas
            ):
            if datas[i+1][key_item_progress] is None:
                pass
            elif ("doxologie" in datas[i+1][key_item_progress].lower()):
                append_doxo[i] = False
            #endIf
        #endIf

    #endFor

    _key_format_priere = "format_priere"
    datas_out = []
    for i in range(len_datas):

        if append_doxo[i]:
            doxologie = _content_doxo[0]
            doxologie[_key_format_priere] = datas[i][_key_format_priere]
            datas_out += [datas[i],]
            datas_out += [doxologie,]
        else:
            datas_out += [datas[i],]
        #endIf

    #endFor

    return datas_out

def collectDatas(
        name,
        progress_datas,
        content_datas,
        LENMAX=1999,
        confiteor=None,
        convertHTML=True,
        personnalisation_datas=None,
        debug=False,
        content_over_progress=None,
        authorize_recursive=True,
        force_format_short_version=None,
        skip_break=False,
        **kwargs
):

    # priorite des titres
    # dans le deroulement : "title"
    # dans les donnees : filename:"title"
    # dans les donnees : "title"

    # comment pouvoir faire un mix entre appel API et donnees dans le fichier ?
    _title_as_reference_progress = "title_as_reference"

    _title_item_content = "titre" #"title"
    _title_item_progress = "name"   #todo : changer en title !

    _key_item_progress = "key"

    _reference_item_content = "reference"
    _reference_item_progress = "reference"

    _text_item_content = "texte"
    _text_item_progress = "texte"

    _short_item_progress = "format_priere"
    _short_item_progress_keywords = ["court","courte","short","tout", "toute", "all"]

    _id_item_progress = "id"

    _confiteor_dominicain = "confiteor_dominicain"
    _confiteor_missel = "confiteor_missel"

    _subtitle_item = "subtitle"  # pour preciser le nom complet a partir de la ref.

    _break_item_progress = "new_section"
    _key_force_skip_doxo = "force_skip_doxo"
    _key_force_skip_antienne = "force_skip_antienne"  #TODO (pour les psaumes en 2 ou 3 parties)

    _author_item_content="auteur"
    _author_item_progress="auteur"

    _editor_item_content="editeur"
    _editor_item_progress="editeur"

    if debug is None:
        debug=globals.debug
    #endIf

    datas_out = []


    if type(content_datas) is str:
        content_datas_items = content_datas
    elif name in content_datas.keys():
        content_datas_items = content_datas[name]
    else:
        content_datas_items = content_datas
    #endIf

    personnalisation = []

    for progress_item in progress_datas:

        # init :
        title_item = None
        reference_item = None
        text_item = None
        short_item = False
        id_item = None
        additional_items = None
        item_content_over_progress = False
        item_break = False
        item_key = None
        text_item_splitted = None
        author_item = None
        editor_item = None
        # Exceptions
        if(type(progress_item)==str):
            continue
        #endIf

        try :

            if _break_item_progress in progress_item.keys():
                item_break = progress_item[_break_item_progress]
            elif not _key_item_progress in  progress_item.keys():
                write_log(
                    user="collectDatas",
                    content="Error (collect_datas.py) : {} not found in {}\n".format(_key_item_progress, progress_item.keys())
                )
            elif progress_item[_key_item_progress] is None:
                item_break = True
            #endIf



            # if "format_priere" in progress_item.keys():
            #     if( (format_priere_court) and
            #         (progress_item["format_priere"] not in ["court","tout","short","all"])
            #        ) :
            #         continue
            #     #endIf
            # #endIf



            if progress_item == {}:
                continue
            #endIF
        except Exception as e:
            sys.stderr.write(str(e)+"\n")
            if(debug):
                sys.stderr.write("collect_datas 0708 - 10\n")
                exit(2)
            #endIf
        #endTry

        try:

            if _title_as_reference_progress in progress_item.keys() :
                item_content_over_progress = progress_item[_title_as_reference_progress]
            #endIf
            if content_over_progress is not None:
                item_content_over_progress = item_content_over_progress or content_over_progress
            #endIf

            # Recuperer le titre :
            title_progress, title_content = titleOrRef_in_item(
                progress_item=progress_item,
                content_datas=content_datas_items,
                key_progress=_title_item_progress,
                key_content=_title_item_content,
                debug=debug
            )

            if title_progress is None:
                title_item = title_content
            else:
                title_item = title_progress
            #endIf

            # Recuperer la reference
            if item_content_over_progress is None or not item_content_over_progress:
                _, reference_item = titleOrRef_in_item(
                    progress_item=progress_item,
                    content_datas=content_datas_items,
                    key_progress=_reference_item_progress,
                    key_content=_reference_item_content,
                    debug=debug
                )
            else:
                reference_item = title_content
                title_item = title_progress
            #endIf

            # Recuperer l'auteur
            _, author_item = titleOrRef_in_item(
                progress_item=progress_item,
                content_datas=content_datas_items,
                key_progress=_author_item_progress,
                key_content=_author_item_content,
                debug=debug
            )

            # Recuperer l'editeur
            _, editor_item = titleOrRef_in_item(
                progress_item=progress_item,
                content_datas=content_datas_items,
                key_progress=_editor_item_progress,
                key_content=_editor_item_content,
                debug=debug
            )


            # Priere courte (True) ou longue
            if force_format_short_version is not None:
                short_item = force_format_short_version
            else:
                if _short_item_progress in progress_item.keys():
                    short_item = progress_item[_short_item_progress] in _short_item_progress_keywords
                else:
                    short_item = True
                #endIf
            #endIf
        except Exception as e:
            sys.stderr.write(str(e)+"\n")
            if(debug):
                sys.stderr.write("collect_datas 0708 - 11\n")
                exit(2)
            #endIf
        #endTry

        if _key_item_progress in progress_item.keys():
            item_key = progress_item[_key_item_progress]
        #endIf

        if not _key_item_progress in progress_item.keys():
            if skip_break:
                continue
            #endIf
            pass
        elif progress_item[_key_item_progress] is None:
            # if _title_item_progress in progress_item.keys():
            #     title_item = progress_item[_title_item_progress]
            # else :
            #     title_item = None
            # #endIf

            # if _reference_item_progress in progress_item.keys():
            #     reference_item = progress_item[_reference_item_progress]
            # else :
            #     reference_item = None
            # #endIf
            if skip_break:
                continue
            #endIf
            pass

        elif progress_item["type"] in (0,1,2,3,4,5,6,7,8,9) or not "type" in progress_item.keys():
            #TODO : changer le type 0 : ca devient ne pas afficher.


            # if(
            #         type(content_datas_items) is dict and
            #         not progress_item[_key_item_progress] in content_datas_items
            # ):
            #     # Ici, on est cense trouver du contenu dans *ce* fichier, mais il n y en a pas...
            #     continue
            # #endIf

            try:
                # Recuperer le contenu
                text_item = text_in_item(
                    progress_item=progress_item,
                    content_datas=content_datas_items,
                    key_progress=_text_item_progress,
                    key_content=_text_item_content,
                    progress_item_key=progress_item[_key_item_progress],
                    debug=debug
                )
            except Exception as e:
                sys.stderr.write(str(e)+"\n")
                if(debug):
                    sys.stderr.write("collect_datas 0708 - 12\n")
                    exit(2)
                #endIf
            #endTry

        elif progress_item["type"] in [10, 11]:

            if progress_item["type"] == 11:
                sys.stderr.write("type 11 depreciated : please use 10 ; ("+str(progress_item)+") in file : "+str(name)+"\n")
            #endIf

            #from collect_datas import contenuCompendium


            # the_content = contenuCompendium(progress_item["key"], convertHTML=False, include=True, **kwargs_transmit)
            # title_item, reference_item, text_item = json2datas

            key_additional=progress_item[_key_item_progress]

            # Mysteres
            if key_additional.lower() in ["mystere", "mystère", "mysteres", "mystères"]:
                if _id_item_progress in  progress_item.keys():
                    try:
                        id_item = progress_item[_id_item_progress]
                        name_mystere = getMystere(**kwargs)

                        # DEBUG
                        id_item = 1
                        name_mystere="lumineux"

                        name_additional="plc_rosary_{myst}_{nb}".format(myst=name_mystere, nb=id_item)
                    except Exception as e:
                        sys.stderr.write(str(e)+"\n")
                        if(debug):
                            sys.stderr.write("collect_datas 0708 - 13\n")
                            exit(2)
                        #endIf
                    #endTry
                #endIf
            elif key_additional == "credits_mystere" :
                name_additional="plc_rosary_credits"
            elif(key_additional.lower() == "confiteor"):
                try:
                    if confiteor_dominicain == True:
                        name_additional = _confiteor_dominicain
                    else:
                        name_additional = _confiteor_missel
                    #endIf
                except :
                    name_additional = _confiteor_missel
                #endIf
            else:
                name_additional = key_additional
            #endIf

            try:
                kwargs_transmit={
                    k:v for k,v in progress_item.items()
                    if k not in ["key", "type", "name"]
                }  #TODO : utiliser les var
                if name != name_additional:
                    # eviter les reccurences
                    #if(debug):
                    #sys.stdout.write("Aller chercher contenu:  {}\n".format(name_additional))
                    additional_items, personnalisation_tmp = json2datas(
                        name=name_additional,
                        debug=debug,
                        content_over_progress=item_content_over_progress,
                        **kwargs_transmit
                    )
                    personnalisation += [personnalisation_tmp,]
                else:
                    sys.stderr.write("Error : infty loop ! (1) \n")
                #endIf
            except Exception as e:
                sys.stderr.write(str(e)+"\n")
                if(debug):
                    sys.stderr.write("collect_datas 0708 - 14\n")
                    exit(2)
                #endIf
            #endTry
        #endIf

        try:
            if additional_items is None:
                pass
            elif additional_items :
                if len(additional_items) == 1 :

                    if item_content_over_progress:
                        if additional_items[0][_title_item_content] is not None and reference_item is None:
                            additional_items[0][_reference_item_content] = additional_items[0][_title_item_content]
                        #endIf
                    #endIf
                    if reference_item is not None :
                        additional_items[0][_reference_item_content] = reference_item
                    #endIf

                    if title_item is not None :
                        additional_items[0][_title_item_content] = title_item
                    #endIf

                    if author_item is not None :
                        additional_items[0][_author_item_content] = author_item
                    #endIf

                    if editor_item is not None :
                        additional_items[0][_editor_item_content] = editor_item
                    #endIf


                else:

                    additional_items= [{
                        _title_item_content:title_item,
                        _reference_item_content:reference_item,
                        _subtitle_item:None,
                        _text_item_content:None,
                        _author_item_content:author_item,
                        _editor_item_progress:editor_item,
                        _short_item_progress:short_item,
                        _break_item_progress:item_break,
                        _key_item_progress:item_key,
                    },] + additional_items

                #endIf

                force_short_include = None
                if force_format_short_version is not None:
                    force_short_include = force_format_short_version
                elif _short_item_progress in progress_item.keys():
                    force_short_include = progress_item[_short_item_progress] in _short_item_progress_keywords
                #endIf
                if force_short_include is not None:
                    for i in range(len(additional_items)):
                        additional_items[i][_short_item_progress] = force_short_include
                    #endFor
                #endIf
                #endFor
            #endIf (additional_items)
        except Exception as e:
            sys.stderr.write(str(e)+"\n")
            if(debug):
                sys.stderr.write("collect_datas 0708 - 15\n")
                exit(2)
            #endIf
        #endTry

        try:
            if additional_items is None :

                if(
                        title_item is None and
                        text_item is None
                ):
                    item_break = True
                #endIf

                if text_item is None or not text_item:
                    text_item_splitted = [None,]
                else:
                    text_item_splitted = [text_item,]
                #endIf

                additional_items = []
                i = 0
                for text_item in text_item_splitted :
                    if len(text_item_splitted) > 1:
                        if type(item_key) in (list, tuple) :
                            if len(item_key>1):
                                sys.stderr.write("json2datas : unexpected : len != 1  : {}\n".format(item_key))
                                if(debug):
                                    exit(2)
                                #endIf
                            else:
                                item_key = item_key[0]
                            #endIf
                        #endIf

                        if type(item_key) is str :
                            item_key = item_key+"_{}".format(i)
                        #endIf
                    #endIf
                    additional_items += [{
                        _title_item_content:title_item if i == 0 else None,
                        _reference_item_content:reference_item if i == 0 else None,
                        _subtitle_item:None,
                        _text_item_content:text_item,
                        _author_item_progress:author_item,
                        _editor_item_progress:editor_item,
                        _short_item_progress:short_item,
                        _break_item_progress:item_break,
                        _key_item_progress:item_key,
                        _key_force_skip_doxo:(i != len(text_item_splitted) - 1)
                    },]
                    i+=1
                #endFor

            #endIf

            datas_out += additional_items

        except Exception as e:
            sys.stderr.write(str(e)+"\n")
            if(debug):
                sys.stderr.write("collect_datas 0708 - 16\n")
                exit(2)
            #endIf
        #endTry



    #endFor

    return datas_out

#endDef

if __name__=="__main__":

    debug = True

    print(sys.builtin_module_names)
    print("* ** *** **** *** ** *")
    print("content offices")
    # getContentOffice :
    office_name = "complies"
    #office_name = "lectures"
    #office_name = "laudes"
    datas_description = readJson('./content_office/{}.json'.format(office_name), key="deroulement")

    #office = aelf2datas(office_name=office_name, the_day="today")
    #out = collectDatas(name=office_name, progress_datas=datas_description, content_datas=office, debug=True)

    print("* ** *** **** *** ** *")
    print("content compendium")

    #name="chapelet"
    name="pour_yaedia"
    #name="pater_noster"
    out0, _ = json2datas(name, debug=debug, personnalisation_key="yaedia")


    # ################

    out=add_doxologie(out0)

    print("vv --------------------------------- vv ---------------------------------- vv")
    #for e in out:
    #    print(e)

