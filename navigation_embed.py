import discord
from discord.ext import commands

import datetime

def generate_dico_embed(
        datas,
        name="",
        name_author="",
        color=0,
        thumbmail_url=None,
        inline=True
):

    _empty = "_ _"

    if(type(datas) is dict):
        datas = [datas,]
    #endIf

    if(type(datas) in [list, tuple]):
        for e in datas:
            if not "inline" in e.keys():
                e["inline"]=False
            #endIf

            for f in ["name", "value"]:
                if f in e.keys():
                    if not e[f]:
                        e[f] = _empty
                    #endIf
                #endIf
            #endFor

        #endFor
    else:
        print("unexpected !")
        return None
    #endIf

    dico = {
        'thumbnail': {'url': thumbmail_url},
        'author': {
            'name': name_author,
            'icon_url': thumbmail_url
        },
        'fields': [
            {
                'inline': inline,
                'name': name if name else _empty,
                'value': _empty
            },
        ] + datas,
        'color': 0x0bab4e,
        'timestamp': str(datetime.datetime.now()),
        'type': 'rich'}

    return dico
#endDef


def page_end(
        name_author="",
        droits="AELF",
):

    dico = generate_dico_embed(
        {"name":"Merci d'avoir utilisé ce bot pour la prière !",
         "value":"À bientôt !\nContenu : tous droits réservés : {}\n".format(droits)
         },
         name_author= name_author
    )
    print(dico)
    return discord.Embed.from_dict(dico)
#endDef



async def navigation_embed(ctx, bot, pages=None, page_end=None):

    _reaction_page_first = '⏮'
    _reaction_page_previous = '◀'
    _reaction_page_next = '▶'
    _reaction_page_last = '⏭'
    _reaction_page_stop = "\N{OCTAGONAL SIGN}"#🛑
    _stop_after_last = False

    if pages is None :
        return None
    elif not pages:
        return None
    elif type(pages) not in [list, tuple]:
        return None
    #endIf

    if _stop_after_last and page_end is not None:
        pages += page_end
    #endIf

    message = await ctx.send(embed = pages[0])

    await message.add_reaction(_reaction_page_first)
    await message.add_reaction(_reaction_page_previous)
    await message.add_reaction(_reaction_page_next)
    await message.add_reaction(_reaction_page_last)
    if page_end is not None :
        await message.add_reaction(_reaction_page_stop)
    #endIf

    i = 0
    reaction = None

    len_pages = len(pages)-1

    def check_reactions(reaction, user):
        return reaction.message.id == message.id and user == ctx.author
    #endDef

    while True:

        if str(reaction) == _reaction_page_first:

            i = 0
            await message.edit(embed = pages[i])

        elif str(reaction) == _reaction_page_previous:

            if i > 0:
                i -= 1
                await message.edit(embed = pages[i])
            #endIf

        elif str(reaction) == _reaction_page_next:

            if i < len_pages:
                i += 1
                await message.edit(embed = pages[i])
            #endIf

        elif str(reaction) == _reaction_page_last:

            i = len_pages
            await message.edit(embed = pages[i])

        elif str(reaction) == _reaction_page_stop :
            if page_end is not None :
                await message.edit(embed = page_end)
            #endIf
            break

        #endIf

        try:

            reaction, user = await bot.wait_for(
                'reaction_add',
                timeout = 1800,
                check = check_reactions
            ) #timout : 1800 s
            await message.remove_reaction(reaction, user)

        except Exception as e:
            print(e)
            if page_end is not None :
                await message.edit(embed = page_end)
            #endIf
            break
        #endTry

    #endWhile

    await message.clear_reactions()

#endDef

if __name__=="__main__":
    d = generate_dico_embed(
        {"name":"coucou",
         "value":"Ceci est un message"
         }
    )

    print(d)
