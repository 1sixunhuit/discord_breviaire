import os

import globals
from write_logs import *
from collect_datas import readJson

def readRestrictionFile(restrictionFile):
    # Priorite sur les arguments de la fonctions, pas sur le fichier
    if restrictionFile is None:

        if globals.restrictionFile is None:
            useFile = None
        else:
            restrictionFile = globals.restrictionFile
            useFile = os.path.exists(globals.restrictionFile)
        #endIf

    else:

        useFile = os.path.exists(globals.restrictionFile)

    #endIf

    # Si le fichier existe : le lire
    if useFile:

        jsonRestrictions = readJson(restrictionFile)

    else:

        content_log = "**Warning**"
        content_log += "None restriction file at {restrictionFile} :"
        content_log += "every command can be used by everyone !"

        write_log(user="Bot", content=content_log)
        jsonRestrictions = None
    #endIf

    return jsonRestrictions, useFile
#endDef


def searchInDico(
        value_name,
        value_id,
        datas,
        key_name="name",
        key_id="id"):

    try:
        v_name = str(value_name) if value_name is not None else None
    except TypeError:
        v_name = None
    except Exception as e:
        sys.stderr.write("Unexpected 3  "+str(e)+"\n")
    #endTry

    try:
        v_id = int(value_id) if value_id is not None else None
    except TypeError:
        v_id = None
    except Exception as e:
        sys.stderr.write("Unexpected 4  "+str(e)+"\n")
    #endTry


    r_len_datas = range(len(datas))
    check_table = [(False, False) for e in r_len_datas]


    for i in r_len_datas:

        e = datas[i]

        try:
            if key_name in e.keys() :
                e_name = str(e[key_name]) if e[key_name] is not None else None
            else :
                None
            #endIf
        except TypeError:
            e_name = None
        except Exception as e:
            sys.stderr.write("Unexpected 1  "+str(e)+"\n")
        #endTry
        try:
            if key_id in e.keys() :
                e_id = int(e[key_id]) if e[key_id] is not None else None
            else :
                None
            #endIf
        except TypeError:
            e_id = None
        except Exception as e:
            sys.stderr.write("Unexpected 2 "+str(e)+"\n")
        #endTry

        check_table[i] = (
            e_name == v_name
            if (e_name is not None and v_name is not None) else None,
            e_id == v_id
            if (e_id is not None and v_id is not None) else None
        )

    #endFor

    if not check_table:
        sum_ok = -1
    else:
        sum_ok = 0
    #endIf

    for e in check_table:

        if e[0] and e[1]:
            return (True, True)
        elif e[0] and sum_ok < 1: # check on names
            sum_ok = 1
        elif e[1] and sum_ok < 2: # check on id
            sum_ok = 2
        #endIf

    #endFor
    if sum_ok == -1:
        check_name = None
        check_id = None
    elif sum_ok == 1:
        check_name = True
        check_id = False
    elif sum_ok == 2 :
        check_name = False
        check_id = True
    else:
        check_name = False
        check_id = False
    #endFor

    return check_name, check_id

#endDef


def allowUsage(ctx,
               restrict=None,
               authorizedUsers=None, authorizedChannels=None, authorizedRoles=None,
               restrictedUsers=None, restrictedChannels=None, restrictedRoles=None,
               restrictionFile=None,
               maintainer=None):
    """Authorize or not to use a command,
    regarding parameters and cfg/restrictions.json"""

    jsonRestrictions, useFile = readRestrictionFile(restrictionFile)
    jsonKeys, _ = readRestrictionFile(globals.configurationFile)
    # ========== Restrictions serveurs ================
    _key_authorized_guilds = "authorized_guilds"
    _key_guild_id = "id"
    _key_guild_name = "name"
    _key_maintainer="maintainer"
    _key_maintainer_name="name"

    authorized_guild_ids_names = None

    if _key_authorized_guilds not in jsonRestrictions.keys():
        authorized_guild_ids_names = []
    elif jsonRestrictions[_key_authorized_guilds] is None:
        authorized_guild_ids_names = []
    elif type(jsonRestrictions[_key_authorized_guilds]) in [list, tuple]:
        authorized_guild_ids_names = jsonRestrictions[_key_authorized_guilds]
    elif type(jsonRestrictions[_key_authorized_guilds]) is dict:
        authorized_guild_ids_names = [ jsonRestrictions[_key_authorized_guilds] , ]
    else:
        authorized_guild_ids_names = []
    #endIf

    if authorized_guild_ids_names:
        check_name, check_id = searchInDico(
            value_name=ctx.message.guild.name,
            value_id=ctx.message.guild.id,
            datas=authorized_guild_ids_names,
            key_name="name",
            key_id="id")
    else:
        check_name, check_id= True, True
    #endIf

    if (check_name is None) and (check_id is None):

        pass

    elif( (not check_name) and (not check_id) ):

        sys.stderr.write("Error : Unauthorized guild !")
        write_log(
            user=ctx.author.name,
            content=F"(restrictions.py), UNAUTHORIZED GUILD {ctx.message.content}, {ctx.message.guild.name} ({ctx.message.guild.id})"
        )
        return False, F"Unauthorized guild !\nPlease contact `{jsonKeys[_key_maintainer][_key_maintainer_name]}` to authorized your server to use this instance\n"

    elif( check_name and (not check_id) ):

        sys.stderr.write(F"(restrictions.py), WARNING : authorized guild on name, but not on ID (usurpation is possible), {ctx.message.guild.name} ({ctx.message.guild.id})")
        write_log(
            user=ctx.author.name,
            content=F"(restrictions.py), WARNING : authorized guild on name, but not on ID (usurpation is possible), {ctx.message.guild.name} ({ctx.message.guild.id})"
        )

    elif( (not check_name) and check_id ):

        write_log(
            user=ctx.author.name,
            content=F"(restrictions.py), INFO : authorized guild on ID, but not on name (there is an error on the configuration file ; not critical), {ctx.message.guild.name} ({ctx.message.guild.id})"
        )

    #endIf

    # ======== Restrictions utilisateurs / channels ==
    # S'il n'y a aucune autorisation a chercher
    if (
            (restrict is None) and
            (authorizedUsers is None) and
            (authorizedChannels is None)
    ):
        return True, ""
    #endIf
    _none="none"
    _all="all"

    _key_auth_user = "authorizedUsers"
    _key_auth_chan = "authorizedChannels"
    _key_auth_role = "authorizedRoles"

    _key_restr_user = "restrictedUsers"
    _key_restr_chan = "restrictedChannels"
    _key_restr_role = "restrictedRoles"

    _value_all_autorized_chan = _all
    _value_all_autorized_user = _all
    _value_maintainer = "_maintainer"



    # Recuperons quelques constantes
    user_id = str(ctx.author.id)  # Pour comparer a id maintainer
    username = str(ctx.author.name)
    userfullname = str(ctx.author)
    userroles = list(e.name for e in ctx.author.roles)
    cmd = str(ctx.message.content)
    channel = str(ctx.message.channel)
    instance = str(ctx.message.guild.name)

    user_is_maintainer = (globals.maintainer_id == user_id)

    if jsonRestrictions is None:  # Aucune restriction
        return True, ""
    #endIf

    def keepOrConvertToNone(var):
        return  None if var in ["",[],None] else var
    #endDef

    def checkKeyInRestriction(dicoRestrictions, restrict, key, defaultKeyMissing="none", defaultValueNone="none", defaultError="none"):
        try:

            if restrict in dicoRestrictions.keys():

                if( type(dicoRestrictions[restrict]) is dict ):
                    if( key in dicoRestrictions[restrict].keys() ):
                        return dicoRestrictions[restrict][key] if  dicoRestrictions[restrict][key] is not None else defaultValueNone
                    #endIf
                #endIf

            elif key in dicoRestrictions.keys():

                return dicoRestrictions[key] if  dicoRestrictions[key] is not None else defaultValueNone

            #endIf

            return defaultKeyMissing

        except Exception as e:

            sys.stderr.write("Error (checkKeyInRestriction) : please check logs!\n")
            write_log(
                user=userfullname,
                content=F"(restrictions.py), unexpected in checkKeyInRestriction: {ctx.message.content} : {e}"
            )
            sys.stderr.write(str(e)+"\n")
            pass

        #endTry
        return defaultError
    #endDef

    authorizedUsers = keepOrConvertToNone(authorizedUsers)

    kwargsAuthorized={
        "defaultKeyMissing":_all,
        "defaultValueNone":_all,
        "defaultError":_none
    }
    kwargsRestricted={
        "defaultKeyMissing":_none,
        "defaultValueNone":_none,
        "defaultError":_none
    }

    if authorizedUsers is None and useFile:
        try:

            authorizedUsers = checkKeyInRestriction(jsonRestrictions, restrict, _key_auth_user, **kwargsAuthorized)
            restrictedUsers = checkKeyInRestriction(jsonRestrictions, restrict, _key_restr_user, **kwargsRestricted)

        except Exception as e:

            sys.stderr.write("Error : please check logs!\n")
            write_log(
                user=userfullname,
                content=F"(restrictions.py), unexpect. 2: {ctx.message.content} : {e}"
            )
            sys.stderr.write(str(e)+"\n")
            pass

        #endTry
    #endIf

    if authorizedUsers is not None:

        if _value_maintainer in authorizedUsers and user_is_maintainer:
            authorizedUsers += [username,]
            True
        #endIf

    #endIf

    authorizedRoles = keepOrConvertToNone(authorizedRoles)

    if authorizedRoles is None and useFile:

        try:

            authorizedRoles = checkKeyInRestriction(jsonRestrictions, restrict, _key_auth_role, **kwargsAuthorized)
            restrictedRoles = checkKeyInRestriction(jsonRestrictions, restrict, _key_restr_role, **kwargsRestricted)

        except Exception as e:

            sys.stderr.write("Error : please check logs!\n")
            write_log(
                user=userfullname,
                content=F"(restrictions.py), unexpect. 3: {ctx.message.content} : {e}"
            )
            sys.stderr.write(str(e)+"\n")
            pass

        #endTry

    #endIf

    authorizedChannels = keepOrConvertToNone(authorizedChannels)

    if authorizedChannels is None and useFile:

        try:

            authorizedChannels = checkKeyInRestriction(jsonRestrictions, restrict, _key_auth_chan, **kwargsAuthorized)
            restrictedChannels = checkKeyInRestriction(jsonRestrictions, restrict, _key_restr_chan, **kwargsRestricted)

        except Exception as e:

            sys.stderr.write("Error : please check logs!\n")
            write_log(
                user=userfullname,
                content=F"(restrictions.py), unexpect. 3: {ctx.message.content} : {e}"
            )
            sys.stderr.write(str(e)+"\n")
            pass

        #endTry

    #endIf

    allow   = True
    message = ""

    rolesAllowed = False
    rolesNotAllowed = False

    if _all in authorizedRoles:
        rolesAllowed = True
    elif _none in authorizedRoles:
        rolesAllowed = False
    else:
        if type(authorizedRoles) is str:
            rolesAllowed = authorizedRoles in [_all,]+ userroles
        elif type(authorizedRoles) in [list, tuple]:
            for e_role in userroles:
                rolesAllowed    = rolesAllowed or ( e_role in authorizedRoles)
            #endFor
        #endIf
    #endIf
    if _all in restrictedRoles:
        rolesNotAllowed = True
    elif _none in restrictedRoles:
        rolesNotAllowed = False
    else:
        if type(restrictedRoles) is str:
            rolesNotAllowed = restrictedRoles in [_all,]+ userroles
        elif type(restrictedRoles) in [list, tuple]:
            for e_role in userroles:
                rolesNotAllowed = rolesNotAllowed or ( e_role in restrictedRoles)
            #endFor
        #endIf
    #endIf

    # on author (! Keep this one in first place) :


    if rolesAllowed:
        allow = True
    elif _all in restrictedUsers:
        allow = False
        message = "Only user with a role : {} can call this command..\n".format(authorizedRoles)
    elif(
            (restrictedUsers is not None)  and (
                (username in restrictedUsers) or
                (userfullname in restrictedUsers) or
                rolesNotAllowed
            )
    ):
        # Utilisateurs qui n'ont specifiquement pas le droit de mettre un message
        allow = False
        message = "This command can not be called by : {}  or the roles {}\n".format(userfullname, authorizedRoles)

    elif  (
            authorizedUsers is not None or
            authorizedRoles is not None
    ):  # ( "user" in restrict ) and ...

        if type(authorizedUsers)==str:
            authorizedUsers = [authorizedUsers,]
        #endIf

        if _none in authorizedUsers:
            allow = False
            message = "Only user with a role : {}  can call this command.\n".format(authorizedRoles)
        elif (
                (username in authorizedUsers) or
                (userfullname in authorizedUsers) or
                (_value_all_autorized_user in authorizedUsers)
        ):
            allow = True
        else:
            allow = False
            message = "This command can only be called by : {} and roles {}\n".format(authorizedUsers, authorizedRoles)
        #endIf

    #endIf

    # on channel

    if _all in restrictedChannels:
        allow = False
        message = "None user is allowed to call the bot in this channel.\n"
    elif(
            (restrictedChannels is not None) and
            (channel in restrictedChannels)
    ):
        # Utilisateurs qui n'ont specifiquement pas le droit de mettre un message
        allow = False
        message = "This command can not be called in : {}\n".format(channel)

    elif (authorizedChannels is not None):  # ( "channel" in restrict ) and ..

        if (type(authorizedChannels)==str):
            authorizedChannels = [authorizedChannels,]
        #endIf

        if _none in authorizedChannels:
            allow = False
            message = "None user is can call the bot in this channel.\n"
        elif (
                (channel in authorizedChannels) or
                (_value_all_autorized_chan in authorizedChannels)
        ):
            allow = allow
        else:
            allow = False
            message += "You can call this command only in one the following channels : {}\n".format(list(authorizedChannels))
        #endIf
    #endIf

    # Message d'erreur
    if globals.logFile is not None:

        if allow and restrict in [globals.restrict_maintainer, globals.restrict_publisher, globals.restrict_notify]:
            write_log(user=userfullname,
                      content=(
                          "User call \'{cmd}\' from the bot in the channel ".format(cmd=cmd)+
                          "{channel} of the instance {inst}".format(
                              channel=channel,
                              inst=instance)
                      )
                      )
        elif not allow:
            write_log(user=userfullname,
                      content=(
                          "Try (unsuccessfully) to call \'{cmd}\' from the bot in the channel ".format(cmd=cmd)+
                          "{channel} of the instance {inst}".format(
                              channel=channel,
                              inst=instance)
                      )
                      )
        #endIf
    #endIf

    return allow, message


if __name__=="__main__":

    datas=[
        {"name":"test0", "id":1},
        {"name":"test1", "id":2},
        {"name":"test0", "id":None},
        {"name":None   , "id":1},
        {"name":"test4", "id":3},
        {"name":"test5", "id":1},
    ]

    datas = None
    print(searchInDico(value_name="test0", value_id="1", datas=datas))
