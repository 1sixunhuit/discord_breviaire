import asyncio
import os
import discord
from discord.ext import commands

import globals
import restrictions
from write_logs import *
from formatStrings import *


#async def restart_bot():
#os.execv(sys.executable, ['python3'] + sys.argv)
#endDef

async def add_vote(ctx, content="Votez !", *emojis: discord.Emoji):
    msg = await ctx.reply(content)
    for emoji in ('👍', '👎'):
        await msg.add_reaction(emoji)
    #endFor
#endDef


async def chat_with_user(bot, ctx,timer=60, iterations=2,restrict=None):

    allowed, msg_content = restrictions.allowUsage(
        ctx,
        restrict=restrict
    )

    if not allowed:
        return await ctx.send("stop {0}".format(msg_content))
    #endIf

    msg = None

    while iterations > 0:

        try:

            msg = await bot.wait_for("message", timeout=timer)  # seconds

        except asyncio.TimeoutError:  # import asyncio if you haven't yet
            # do something if we haven't received a message

            if iterations > 1:
                await ctx.send(F""":warning: :timer: Pas de donnée entrée pendant ces premières {timer} secondes)
Il ne reste que {timer} secondes avant de devoir recommencer !""")
            else:
                await ctx.send(F""":warning: :timer: Pas de donnée entrée pendant ces {timer} nouvelles secondes)
**Arrêt : vous pouvez relancer la commande** pour recommencer !
__*Un conseil*__ : Préparez le contenu dans un bloc-note avant de relancer la commande et copiez-collez celui-ci :wink:""")

                return None
            #endIf

        else:
            # received a message within n seconds

            if msg.content.lower() in ["stop",]:

                await ctx.send("STOP")
                return None

            #endIf
        #endTry

        iterations = iterations - 1

        if msg is not None:
            break
        #endIf

    #endWhile

    return msg

#enDef
# ================================================================== #
# =================== BOT'S FUNCTIONS ============================== #
# ================================================================== #
class Bot_commands(commands.Cog,name="Support"):
    """Commandes bot"""
    def __init__(self, bot):
        self.bot = bot
    #endDef


    @commands.command(
        name="bot_about",
        aliases=["about",],
        description=F"À propos du bot {globals.name}",
        ignore_extra=True,
        hidden=False,
        enabled=True
    )
    async def _about(self, ctx):
        """À propos de l'application"""

        allowed, msg = restrictions.allowUsage(ctx, restrict=globals.restrict_user)
        if not allowed:
            await ctx.send(":warning: {0}".format(msg))
            return 6
        #endIf

        output=":warning: Todo : liste des commandes support (info, bug, request, helloU, ...)\n"
        output+=globals.description
        output+="""\n\n>>> Code python et support disponible sur
https://framagit.org/1sixunhuit/discord_breviaire
Merci d'y ouvrir un ticket en cas de bug ou pour toute demande.
Le développeur décline toute responsabilité à l'usage de cette application, bien qu'il ait fait de son mieux.

Amen !"""

        await ctx.send(output)
    #endDef


    @commands.command(
        name="bot_credits",
        aliases=["credits",],
        short=F"Crédits",
        description=F"Crédits",
        ignore_extra=True,
        hidden=False,
        enabled=True
    )
    async def _credits(self, ctx):
        """À propos de l'application"""
        allowed, msg = restrictions.allowUsage(ctx, restrict=globals.restrict_user)
        if not allowed:
            await ctx.send(":warning: {0}".format(msg))
            return 6
        #endIf

        await ctx.send("""Cette application ("bot") utilise les contenus d'autres services.
        **Tous droits réservés aux ayants-droits respectifs.**

>>>    Liturgie des heures : AELF (<https://aelf.org>)
    Messe, Évangile     : AELF (<https://aelf.org>)

    Prière pour Yaedia  : AELF (psaumes), Compendium du catéchisme de l'Église catholique (<https://www.vatican.va/>)

    Confiteor           : Wikipedia (<https://fr.wikipedia.org/>)
    Confiteor dominicain: Wikipedia (<https://fr.wikipedia.org/>)

    Doxologie des heures: Wikipedia (<https://fr.wikipedia.org/>)

    Angélus             : Wikipedia (<https://fr.wikipedia.org/>)
    Angélus chanté      : Wikipedia (<https://fr.wikipedia.org/>)
    Salve Regina        : Wikipedia (<https://fr.wikipedia.org/>)
    Regina Cæli         : Wikipedia (<https://fr.wikipedia.org/>)

    Notre Père          : CEF (<https://eglise.catholique.fr/>)
    Je vous salue Marie : CEF (<https://eglise.catholique.fr/>)

    Symbôle des Apôtres (credo)   : CEF (<https://eglise.catholique.fr/>)
    Credo de Nicée-Constantinople : CEF (<https://eglise.catholique.fr/>)

    Acte de contrition (court): Vatican News (<https://www.vaticannews.va/fr>)
    Acte de contrition (long) : CEF (<https://eglise.catholique.fr/>)

    Mystères du rosaire : Prier le chapelet (<https://www.prierlechapelet.com/>)

Le code a été développé par @pmaltey et est sous license GNU Affero General Public License (voir le fichier LICENSE)
""")
    #endDef


    @commands.command(
        name="bot_bug",
        aliases=["bug",],
        brief=F"Signaler un bug",
        description="Signaler un bug ; écrire un commentaire derrière la commande",
        hidden=False,
        enabled=True
    )
    async def _bug(self, ctx):
        """Signaler un bug (ajouter la description à la suite de la commande)"""
        allowed, msg = restrictions.allowUsage(ctx, restrict=globals.restrict_notify)
        if not allowed:
            await ctx.send(":warning: {0}".format(msg))
            return 6
        #endIf

        write_log(
            user=ctx.author,
            content="BUG : \'{}\'".format(ctx),
            theFile=globals.logFile
        )

        maintainer_id = await self.bot.fetch_user(globals.maintainer_id)
        await ctx.send(F"Merci ! Le bug a été transmis.\n(pour notification : {maintainer_id.mention})")
    #endDef


    @commands.command(
        name="bot_request",
        aliases=["request",],
        brief=F"Demander une nouvelle fonctionnalité",
        description="Demander une nouvelle fonctionnalité ; écrire un commentaire derrière la commande",
        hidden=False,
        enabled=True
    )
    async def _request(self, ctx):
        """Demander une implémentation"""
        allowed, msg = restrictions.allowUsage(ctx, restrict=globals.restrict_notify)
        if not allowed:
            await ctx.send(":warning: {0}".format(msg))
            return 6
        #endIf
        write_log(
            user=ctx.author,
            content="REQUEST : Creat the command : \'{}\'".format(ctx.message.content.replace("?request",""))
        )

        maintainer_id = await self.bot.fetch_user(globals.maintainer_id)

        await ctx.send(F"La demande a été enregistrée.\n(pour notification : {maintainer_id.mention})")
    #endDef

#endClass


def setup(bot):
    bot.add_cog(Bot_commands(bot))
#endDef
