# Bot bréviaire

## Description

Ce bot sert de support à la prière régulière (ou ponctuelle) dans la foi catholique selon le rite romaine en français.

Voir le paragraphe "Crédits" pour les sources.

## Versions

### Passées
* v0.1.0 : premiers tests
* v0.2.0 : séparation des fichiers sources entre bot, traitement de la donnee et traitement des chaines de caractere
* v0.3.0 : utilisation de fichiers json pour les deroulements
* v0.4.0 : ajout de l'Evangile du jour
* v0.4.1 : Correction mise en forme des offices (saut de ligne, nouveau message)
* v0.4.2 : Gras sur ponctuation psalmaudique + suppression des espaces en fin de ligne
* v0.5.0 : Ajout de l'Angelus et du Regina Caeli
* v0.5.1 : Debug : gestion des accents HTML + suppresion de l'acclamation apres l'Evangile + ajout alias pour Regina Caeli
* v0.5.2 : Legere refonte de la facon de concatener les messages + ajout alias reginacaeli + creation log + debug
* v0.5.3 : Correction bug : titre patristrique devient le titre du texte patristique
* v0.6.0 : Ajout de la doxologie apres les psaumes/cantiques + confiteor dans les complies
* v0.6.1 : Implementation de securites pour restreindre l'usage du bot
* v0.6.2 : Debug (re-enlever l'acclamation a la fin de l'Ev.), changt appel confiteor complies, suppression lead/trail spaces, modification nom dossier
* v0.6.3 : Factorisation des appels au bot + système de votes lié aux logs pour signaler un soucis + ajout prière defunt (Yaedia)
* v0.6.4 : Ajout possibilité prière raccourcie (Yaedia), ajout logs, ajout Antienne Zacharie, amélioration formattage texte
* v0.6.5 : Ajout Salve Regina + maj template key.json

### En debug
* v0.7.0 : Ajout "avant la fin de la lumiere" + Crédits + ?breviaire_credits + notif mainteneur avec sign. bug + refactoring
* v0.7.1 : Ajouter des prieres dans Discord directement
* v0.7.2 : Debug ajout des prieres + restart modules + separation user/admin + move config files into cfg and template in _cfg + update gitignore + va chercher le chemin auto entre additionnals et compendium
* v0.7.3 : Monitorage propre + gestion des droits entierement implementee + decorateurs modifies
* v0.7.4 : Personnalisation priere defunt
* v0.7.5 : possibilite d'appeler une priere du compendium dans n'importe quelle autre priere (type = 10)
* v0.7.6 : Chapelet + split antienne sur ponctuation
* v0.7.7 : Debug mise en forme (doublons tags) + ecriture de collectDatas() pour soulager jsondatas2message() + restriction sur serveurs implementee

### En développement

### Future
* v0.8.0 : utilisation des embed
* v0.8.1 : Ajout de la messe
* v0.9 : Refonte de la fonction de split
* v1.0 : Release stable

## Installation
### Prérequis (logiciels)
* python3
* python3-venv (optionnel, pour l'utilisation d'un environnement virtuel dédié)

### Prérequis (libraires Python)
* discord.py[voice]
* requests


### Installation

    $ python3 -m venv discord
    $ source discord/bin/activate
    $ pip install discord.py[voice] requests
    OR
    $ pip install -r ./requirements_python.txt

    $ mv _cfg cfg

* Spécification des autorisations dans cfg/restriction.json:
  * restrictedUsers/Channels : (est prioritaire sur authorizedUsers/Channels
     * null : personne n'est spécifiquement interdit
     * "user1" : seul l'utilisateur "user1" est bloqué (! peu sécurisé : homonymes possibles)
     * "user1#id" : seul l'utilisateur "user1" avec un id est bloqué

  * authorizedUser/Channels :
     * null : tous les utilisateurs/channels sont autorisé à avoir accès à la commande
     * "user1" : seul un utilisateur nommé user1 peut y avoir accès (! peu sécurisé : homonymes possibles)
     * "user1#id" : seul l'utilisateur correspondant avec ce pseudo et cet id peut y avoir accès
     * "_maintainer" : seul le mainteneur peut y avoir accès.
     * (todo : groupe)
  * Note : chaque clé correspond a un module particulier, sauf pour user et notify qui correspondent tous les deux à bot_user
* Spécifications de key.json
  * token : jeton du bot
  * urlAdd : URL pour ajouter le bot (inutile, mais permet de la conserver sous la main)
  * maintainer
    * name : pseudo Discord du mainteneur
    * id   : numéro d'identification unique du mainteneur

### Utilisation
* Lancement du serveur, dans le dossier avec le programme Python :

    $ python3 breviaire_bot.py

* Dans Discord, appeler l'une des commandes suivantes

* Bot
    > ?help
    > ?breviaire_infos
    > ?breviaire_bug (dire ce qui ne va pas) : ATTENTION : pas certian que ca fonctionne
    > ?sommaire                : Afficher la liste des offices, prières etc
    > ?request                 : Demander qqc aux mainteneur du bot

* Offices
    > ?laudes
    > ?lectures
    > ?sixte
    > ?none
    > ?vepres
    > ?complies                : avec confiteor du missel
    > ?complies confiteor      : avec              missel
    > ?complies missel         : avec              missel
    > ?complies dominicain     : avec              dominicains
    > ?complies sans confiteor : sans confiteor

* Prières
    > ?angelus
    > ?angelus_chant           : Angélus chanté
    > ?regina_caeli
    > ?reginacaeli             : alias pour (?regina_caeli)
    > ?yaedia                  : Prière pour Yaedia (version longue)
    > ?yaedia courte           :                    (version courte)

* Autre :
    > ?evangile                : Évangile de la messe du jour

* À venir :
    > ?messe            (To do)
    > ?psaume           (To do): Psaume de la messe du jour
    > ?salve_regina

## To-do list
### Questions :
* Quand il n'y a pas d'antienne a un psaume, ca se passe comment ?


### Divers :
* Logo a refaire (droits)

### Code :
#### Usage :
* Incorporer la messe + le psaume du jour uniquement
* Appeler extrait d'un office
* help perso https://stackoverflow.com/questions/64333089/discord-py-rewrite-custom-help-embed-sort-commands-by-cog
* Prière pour Yaedia : PAsser à Marie-Charlotte (et généraliser H/F ?)
* restrictions.json : gerer les groupes
* Pour les paroles de Dieu : ajouter le nom complet (Mt : Mattheiu par ex)

#### Admin :
* Monitorage utilisation/erreur
#### Code :
* Rendre lisible fonction de split
* Vérifier tout le contenu avec le site AELF => vérifier que toutes les clés ont été utilisées
* Ne pas planter si une clé n'existe pas mais sortir un message d'erreur


## Fichiers de configuration
### AELF

### Prieres supplementaires

### Restrictions

* Restrict on channels only (for exemple)

   "restrict_xxx":{
       "authorizedUsers":null,
       "authorizedChannels":["chan_1","chan_2"]
   }

* ! authorizedUser**s**, authorizedChannel**s**,

## Documentation
* https://discordpy.readthedocs.io/en/stable/api.html


## Crédits
Cette application ("bot") utilise les contenus d'autres services.
**Tous droits réservés aux ayants-droits respectifs.**

    Liturgie des heures : AELF (<https://aelf.org>)
    Messe, Évangile     : AELF (<https://aelf.org>)

    Prière pour Yaedia  : AELF (psaumes), Compendium du catéchisme de l'Église catholique (<https://www.vatican.va/>)

    Confiteor           : Wikipedia (<https://fr.wikipedia.org/>)
    Confiteor dominicain: Wikipedia (<https://fr.wikipedia.org/>)

    Doxologie des heures: Wikipedia (<https://fr.wikipedia.org/>)

    Angélus             : Wikipedia (<https://fr.wikipedia.org/>)
    Angélus chanté      : Wikipedia (<https://fr.wikipedia.org/>)
    Salve Regina        : Wikipedia (<https://fr.wikipedia.org/>)
    Regina Cæli         : Wikipedia (<https://fr.wikipedia.org/>)

    Notre Père          : CEF (<https://eglise.catholique.fr/>)
    Je vous salue Marie : CEF (<https://eglise.catholique.fr/>)

    Symbôle des Apôtres (credo)   : CEF (<https://eglise.catholique.fr/>)
    Credo de Nicée-Constantinople : CEF (<https://eglise.catholique.fr/>)

    Acte de contrition (court): Vatican News (<https://www.vaticannews.va/fr>)
    Acte de contrition (long) : CEF (<https://eglise.catholique.fr/>)

    Mystères du rosaire : Prier le chapelet (<https://www.prierlechapelet.com/>)

# Bug :
## Discord, voice channel (text)


# Docs :
# https://zestedesavoir.com/articles/1568/decouvrons-la-programmation-asynchrone-en-python/

# Git :
## Nomenclature :

* main : branche principale (stable en principe)
* dev  : depreciee : **à faire** : merger les branches dedans et y faire les tests avant de merger dans le main
* v0_7_0 : version de developpement des versions 0.7 & co : actuellement : refactoring de jsondatas2message dans collectDatas, text_in_item, titleOrRef_in_item
* feat_xxx : ajout d'une nouvelle fonctionnalitee
* refact_xxx : reecriture de fonctions
* bug_xxx : correction d'une erreur